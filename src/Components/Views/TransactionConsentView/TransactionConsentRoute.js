import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import TransactionConsentContainer from "./TransactionConsentContainer";


const TransactionConsentRoute = createStackNavigator({
  TransactionConsentView: {
    screen: TransactionConsentContainer,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default TransactionConsentRoute;
