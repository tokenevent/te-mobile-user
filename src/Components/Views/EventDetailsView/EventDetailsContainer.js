import React, { Component } from 'react';
import { ToastAndroid, View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import EventDetailsView from './EventDetailsView';
import EventsService from '../../../Services/EventsService';
import LoadingIndicator from "../../commons/LoadingIndicator";
import globalStylesheet from "../../../Utils/globalStylesheet";
import FloatingButton from "../../FloatingButton/FloatingButton";

export default class EventDetailsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event: [],
      isLoading: true
    };
  }

  prepareData() {
    EventsService.getEvent(this.props.navigation.getParam('eventId'))
      .then(response => this.eventsLoadSuccessHandler(response))
      .catch(error => this.eventsLoadErrorHandler(error));
  }

  eventsLoadSuccessHandler = response => {
    this.setState({
      event: response.data,
      isLoading: false
    });
  };

  eventsLoadErrorHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas pobierania danych wydarzenia!", ToastAndroid.SHORT);
    console.log(error);
  };

  render() {
    const { isLoading } = this.state;

    return (<View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onWillFocus={() => {
            this.setState({ isLoading: true });
            this.prepareData();
          }}
        />
        {isLoading && <LoadingIndicator/>}

        {!isLoading && <>
          <FloatingButton navigation={this.props.navigation} withBottomButton={this.state.event.id !== 'c0a9ee5c-57df-459d-b980-7375e64358bb'} />
          <EventDetailsView navigation={this.props.navigation} eventData={this.state.event}/>
        </>}
      </View>
    );
  }
}
