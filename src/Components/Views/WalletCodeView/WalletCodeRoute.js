import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import WalletCodeScreen from "./WalletCodeScreen";


const WalletCodeRoute = createStackNavigator({
  WalletCodeView: {
    screen: WalletCodeScreen,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default WalletCodeRoute;
