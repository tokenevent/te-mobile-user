import React, { useEffect } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import failedImage from '../../../../assets/images/close.png';
import successImage from '../../../../assets/images/success.png';
import promoImage from '../../../../assets/images/promoImage.png';
import styles from './PurchaseHistoryRow.style';
import TransactionService from "../../../Services/TransactionsService";
import LoadingIndicator from "../../commons/LoadingIndicator";
import TransactionConsentBox from "../../TransactionConsentBox/TransactionConsentBox";

const PurchaseHistoryRow = ({ purchase, buttonsHighlight }) => {
  const purchaseDate = `${new Date(purchase.createdAt).toLocaleDateString()}, ${new Date(purchase.createdAt).toLocaleTimeString()}`;

  const [isDetailsExpanded, setExpand] = React.useState(false);
  const [isLoading, setLoading] = React.useState(false);
  const [transactionDetails, setTransactionDetails] = React.useState(undefined);

  useEffect(() => {
    setExpand(false)
  }, [buttonsHighlight]);

  const getDetails = id => {
    if (!(transactionDetails && transactionDetails.eventId)) {
      setLoading(true);
      TransactionService.getBuyTransactionDetails(id)
        .then(response => {
          setLoading(false);
          setTransactionDetails(response.data);
        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        });
    }
    setExpand(!isDetailsExpanded)
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.transaction}
        onPress={() => {
          getDetails(purchase.id)
        }}>
        <View style={styles.container_token}>
          <Text>
            Ilość
          </Text>
          <Text style={{ fontWeight: 'bold' }}>
            {purchase.amount}
          </Text>
        </View>

        <View style={styles.container_text}>
          <Text style={styles.title}>
            {purchase.eventName}
          </Text>
          <Text style={styles.description}>
            Wartość zakupów: {purchase.totalValue / 100}{" zł\n"}
            Data transakcji: {purchaseDate}
          </Text>
        </View>
        {purchase.status === "COMPLETED"
        &&
        <View>
          <Image
            style={styles.successImg}
            source={successImage}
            resizeMode="contain"
          />
          <Text style={styles.successDesc}>
            ZAKOŃCZONY
          </Text>
        </View>
        }
        {purchase.status === "CANCELED"
        &&
        <View>
          <Image
            style={styles.failedImg}
            source={failedImage}
            resizeMode="contain"
          />
          <Text style={styles.failDesc}>
            ANULOWANY
          </Text>
        </View>
        }
        {purchase.status === "PROMOTION"
        &&
        <View>
          <Image
            style={styles.failedImg}
            source={promoImage}
            resizeMode="contain"
          />
          <Text style={styles.promotion}>
            KOD PROMOCYJNY
          </Text>
        </View>
        }
      </TouchableOpacity>
      {isDetailsExpanded && !isLoading && transactionDetails && transactionDetails.tokens && transactionDetails.tokens.length > 0 &&
      (transactionDetails.tokens || []).map((t, index) => {
        return <TransactionConsentBox
          key={index}
          amount={t.amount}
          unitPrice={t.unitPrice}
          tokenName={t.buyableTokenName}
        />
      })
      }
      {isLoading && <LoadingIndicator withMargin={false} loadingText="Wczytywanie szczegółów..."/>}
    </View>
  );
};


export default PurchaseHistoryRow;
