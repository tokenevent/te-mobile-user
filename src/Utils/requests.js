import axios from 'axios';
import deviceStorage from "./deviceStorage";

export const authorizeRequest = async () => {
    const token = await deviceStorage.getItem('accessToken');
    return axios.create({
        timeout: 2000,
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
    });
};

export const request = () => {
    return axios.create({timeout: 2000});
};