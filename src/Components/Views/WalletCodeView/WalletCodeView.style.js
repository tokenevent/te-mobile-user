import { StyleSheet } from "react-native";
import globalStylesheet from "../../../Utils/globalStylesheet";

const styles = StyleSheet.create({
  deadCodeHeader: {
    fontFamily: 'TwCenMT-Condensed',
    color: 'black',
    fontSize: 65,
    textTransform: 'uppercase'
  },
  deadCodeDesc: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 35
  },
  blueButton: disabled => {
    return {
      ...globalStylesheet.normalBlueButtonContainer,
      borderRadius: 0,
      opacity: disabled ? 0.5 : 1
    }
  },
  description: {
    color: '#0b3a66',
    fontSize: 16,
    textAlign: 'center'
  },
  deadCodeWrapper: { flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center', alignContent: 'center' },
  codeWrapper: { display: 'flex', alignItems: 'center' },
  secondsText: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 34,
    marginBottom: 15,
    marginTop: 10,
  },
  codeText: {
    fontFamily: 'TwCenMT-Condensed',
    color: 'black',
    fontSize: 60
  }
});
export default styles;

