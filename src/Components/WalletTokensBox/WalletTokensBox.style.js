import { StyleSheet } from "react-native";
import globalStylesheet from "../../Utils/globalStylesheet";

const styles = StyleSheet.create({
  orderedAmount: {
    ...globalStylesheet.orderedAmount,
    marginLeft: 2,
    marginRight: 2
  },
  blueButton: {
    ...globalStylesheet.normalBlueButtonContainer,
    borderRadius: 0
  },
  mainWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 5,
    alignItems: 'center'
  },
  columnWrapper: { display: 'flex', flexDirection: 'column' },
  rowWrapper: { display: 'flex', flexDirection: 'row', alignItems: 'center' }
});
export default styles;

