// const apiUrl = "http://192.168.1.79:8080"; //url lokalne api
const apiUrl = "https://tokeneventapp.herokuapp.com"; //url zew api

export const api = {
    ALL_EVENTS: apiUrl + "/events",
    SINGLE_EVENTS: apiUrl + "/events/",
    LOGIN_USER: apiUrl + "/public/users/login",
    REGISTER_USER: apiUrl + "/public/users/register",
    WALLET_INFO: apiUrl + "/users/me",
    ISSUE_TOKENS: apiUrl + "/tokens/payu",
    WALLET_ID: apiUrl + "/users/me/wallet",
    MY_TRANSACTIONS: apiUrl + "/my/transactions-history",
    TRANSACTION_CONSENT: apiUrl + "/my/transactions",
    TRANSACTION_DETAILS: apiUrl + "/transaction-details",
    PURCHASES_HISTORY: apiUrl + "/my/buy-token-history",
    UPDATE_USER_INFO: apiUrl + "/users/my/settings",
    GET_USER_INFO: apiUrl + "/users/me",
    BUY_TRANSACTION_DETAILS: apiUrl + "/my/buy-token-history"
};
