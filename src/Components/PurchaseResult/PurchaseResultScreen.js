import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import failedImage from '../../../assets/images/close.png';
import successImage from '../../../assets/images/success.png';
import styles from './PurchaseResultScreen.style';
import globalStylesheet from "../../Utils/globalStylesheet";


const SUCCESS = "COMPLETED";
const SUCCESS_HEADER = "Sukces";
const SUCCESS_DESC = "Udało się zakupić żetony!";

const CANCELED = "CANCELED";
const CANCEL_HEADER = "Anulowano";
const CANCELED_DESC = "Zakup anulowano.";

export default class PurchaseResultScreen extends Component {

  onProceed = () => this.props.navigation.navigate('MainView');

  render() {
    const status = this.props.navigation.getParam('result');
    return (
      <View style={globalStylesheet.fullFlex}>
        <View style={styles.infoWrapper}>

          <Image
            style={status === SUCCESS ? styles.successImg : styles.failImg}
            source={status === SUCCESS ? successImage : failedImage}
            resizeMode="contain"
          />

          <Text style={styles.resultHeader}>
            {status === SUCCESS && SUCCESS_HEADER}
            {status === CANCELED && CANCEL_HEADER}
          </Text>
          <Text style={styles.resultDesc}>
            {status === SUCCESS && SUCCESS_DESC}
            {status === CANCELED && CANCELED_DESC}
          </Text>
        </View>
        <View>
          <TouchableOpacity
            onPress={this.onProceed}
            style={globalStylesheet.normalBlueButtonContainerWithoutRadius}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>Wróć</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

