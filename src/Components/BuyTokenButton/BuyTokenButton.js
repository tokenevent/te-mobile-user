import { Text, TouchableOpacity } from "react-native";
import React from "react";
import styles from './BuyTokenButton.style';

const BuyTokenButton = ({ borderColor, text, index, onClick }) => {
  return <TouchableOpacity style={styles.buttonWrapper(borderColor)} onPress={() => onClick(index, Number(text))}>
    <Text style={styles.text}>{text}</Text>
  </TouchableOpacity>;
};

export default BuyTokenButton
