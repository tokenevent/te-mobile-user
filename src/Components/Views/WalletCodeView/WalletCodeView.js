import React, { Component } from 'react';
import { Dimensions, Text, TouchableOpacity, View } from 'react-native';
import globalStylesheet from "../../../Utils/globalStylesheet";
import { NavigationEvents } from 'react-navigation';
import QRCode from 'react-native-qrcode-svg';
import styles from './WalletCodeView.style';

const WIDTH = Dimensions.get('window').width;


export default class WalletCodeView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seconds: this.props.walletCodeInfo.validForSeconds
    }
  }

  refreshCode = () => {
    //Potrzebne wywołanie ponieważ nie można przejść z WalletCode do WalletCode
    this.props.navigation.navigate('MainView');
    this.props.navigation.navigate('WalletCodeView');
  };

  componentDidMount() {
    this.subtractSeconds();
  }

  subtractSeconds = () => {
    this.interval = setInterval(() => {
      if (this.state.seconds > 0)
        this.setState({
          seconds: this.state.seconds - 1
        });
      else {
        clearInterval(this.interval);
        this.interval = null;
      }

    }, 1000);
  };

  renderCodeNotActiveView = () => {
    return <View
      style={styles.deadCodeWrapper}>
      <Text style={styles.deadCodeHeader}>Kod wygasł</Text>
      <Text style={styles.deadCodeDesc}>Wygeneruj nowy kod</Text>
    </View>;
  };

  renderCodeView = walletCodeInfo => {
    return <View style={styles.codeWrapper}>
      <Text style={styles.secondsText}>{this.state.seconds} sekund</Text>
      <QRCode
        value={walletCodeInfo.id}
        size={WIDTH*0.55}
      />
      <Text style={styles.codeText}>
        {walletCodeInfo.id}
      </Text>
    </View>;
  };

  render() {
    const { walletCodeInfo } = this.props;
    const { seconds } = this.state;

    return (
      <View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onDidFocus={() => {
            this.subtractSeconds();
          }}
          onWillBlur={() => {
            clearInterval(this.interval);
            this.interval = null;
          }}
        />
        <View style={globalStylesheet.viewTitleWrapper}>
          <Text style={globalStylesheet.viewTitle}>Kod portfela</Text>
        </View>
        <View style={globalStylesheet.fullFlex}>
          {this.state.seconds > 0 ? this.renderCodeView(walletCodeInfo) : this.renderCodeNotActiveView()}
        </View>
        <TouchableOpacity
          style={styles.blueButton(seconds > 0)}
          disabled={seconds > 0}
          onPress={this.refreshCode}>
          <Text style={globalStylesheet.normalBlueButtonText}>
            Generuj nowy kod
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
