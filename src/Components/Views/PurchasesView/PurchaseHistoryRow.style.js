import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  title: {
    fontSize: 20,
    color: '#000',
  },
  container_text: {
    flex: 3,
    paddingLeft: 8
  },
  container_token: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container_icon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  description: {
    fontSize: 11,
    fontStyle: 'italic',
  },
  successDesc: {
    fontSize: 11,
    color: 'green'
  },
  failDesc: {
    fontSize: 11,
    color: 'red'
  },
  successImg: {
    alignSelf: 'center',
    width: 32,
    height: 32,
    margin: 5
  },
  failedImg: {
    alignSelf: 'center',
    width: 32,
    height: 32,
    margin: 5
  },
  promotion: {
    fontSize: 11,
    color: '#C466C3'
  },
  transaction: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF'
  },
});
export default styles;
