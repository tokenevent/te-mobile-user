import { api } from "../Utils/api-urls";
import { authorizeRequest } from "../Utils/requests";

const PurchaseService = {
    getMyPurchases: () => getMyPurchases(),
    getPurchaseStatus: (id) => getPurchaseStatus(id)
};

const getMyPurchases = () => {
    const url = api.PURCHASES_HISTORY;
    return authorizeRequest().then(a => a.get(url));    
};

const getPurchaseStatus = (id) => {
    const url = api.PURCHASES_HISTORY + "/" + id;
    return authorizeRequest().then(a => a.get(url));
}

export default PurchaseService;
