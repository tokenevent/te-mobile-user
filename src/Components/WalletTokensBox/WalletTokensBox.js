import React, { Component } from 'react';
import { Text, View } from 'react-native';
import globalStylesheet from "../../Utils/globalStylesheet";
import styles from './WalletTokensBox.style';

export default class WalletTokensBox extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { tokensInfo } = this.props;
    const { tokensName } = this.props;

    return (
      <View style={styles.mainWrapper}>
        <View style={styles.columnWrapper}>
          <View style={styles.rowWrapper}>
            <Text style={globalStylesheet.tokenName}>{tokensName}</Text>
            <Text style={globalStylesheet.slash}>/</Text>
            <Text style={globalStylesheet.unitPrice}>{(tokensInfo[0].tokenValue / 100)} zł</Text>
          </View>
          <View style={styles.rowWrapper}>
            <Text style={globalStylesheet.ordered}>Wartość:</Text>
            <Text style={styles.orderedAmount}>{(tokensInfo[0].tokenValue / 100) * tokensInfo.length}</Text>
            <Text style={globalStylesheet.szt}> zł</Text>
          </View>
        </View>
        <View style={styles.rowWrapper}>
          <Text style={globalStylesheet.availableTokens}>{tokensInfo.length}</Text>
          <Text style={globalStylesheet.availableQty}> szt.</Text>
        </View>
      </View>
    )
  }
}
