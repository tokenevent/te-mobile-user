import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import PurchaseResultScreen from "./PurchaseResultScreen";
import DrawerMenu from "../DrawerMenu/DrawerMenu";
import Header from "../Header/Header";


const PurchaseResultRoute = createStackNavigator({
  PurchaseResultView: {
    screen: PurchaseResultScreen,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default PurchaseResultRoute;
