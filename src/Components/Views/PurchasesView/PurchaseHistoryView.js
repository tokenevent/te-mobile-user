import React, { Component } from 'react';
import { View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import PurchaseHistoryListView from "./PurchaseHistoryListView";
import PurchaseService from "./../../../Services/PurchaseService"
import LoadingIndicator from "../../commons/LoadingIndicator";
import ErrorIndicator from "../../commons/ErrorIndicator";
import styles from './PurchaseHistoryView.style';
import globalStylesheet from "../../../Utils/globalStylesheet";
import FloatingButton from "../../FloatingButton/FloatingButton";

export default class PurchaseHistoryView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      purchases: [],
      isLoading: true,
      error: null,
      refreshList: false,
    }
  }

  getData() {
    PurchaseService.getMyPurchases()
      .then(response => this.loadPurchases(response))
      .catch(error => this.handleError(error))
  }

  loadPurchases(httpResponse) {
    this.setState({
      purchases: httpResponse.data.list,
      isLoading: false,
      isError: false
    });
  }

  handleError(err) {
    this.setState({
      purchases: [],
      isLoading: false,
      error: err
    })
    console.log(error);
  }

  getRejectedOnly = (purchase) => purchase.status === "CANCELED";

  getAcceptedOnly = (purchase) => purchase.status === "COMPLETED";

  render() {
    const { isLoading, error, purchases, refreshList } = this.state;
    const isError = error !== null;

    return (
      <View style={styles.wrapper}>
        <View style={globalStylesheet.fullFlex}>
          <NavigationEvents
            onWillFocus={() => {
              this.setState({ isLoading: true });
              this.getData();
            }}
          />
          {isLoading && <LoadingIndicator/>}
          {!isLoading && <><PurchaseHistoryListView
            purchases={purchases}
            getRejectedOnly={this.getRejectedOnly}
            getAcceptedOnly={this.getAcceptedOnly}
            refreshList={refreshList}
          />
            <FloatingButton navigation={this.props.navigation}/>
          </>}

          {isError && !isLoading && <ErrorIndicator/>}

        </View>
      </View>
    );
  }
};
