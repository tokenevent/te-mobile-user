import React from 'react';
import { FlatList, Text, TouchableOpacity, View } from 'react-native';
import PurchaseHistoryRow from "./PurchaseHistoryRow";
import styles from './PurchaseHistoryListView.style'
import globalStylesheet from "../../../Utils/globalStylesheet";


const PurchaseHistoryListView = ({ purchases, getAcceptedOnly, getRejectedOnly }) => {
  const [refreshList, setRefreshList] = React.useState(false);
  const [renderPurchases, setRenderPurchases] = React.useState(purchases);
  const [buttonsHighlight, setButtonsHighlights] = React.useState([true, false, false]);

  return (
    <View style={styles.container}>
      <View style={globalStylesheet.viewTitleWrapper}>
        <Text style={globalStylesheet.viewTitle}>Historia zakupów</Text>
      </View>

      { purchases && purchases.length > 0 ?
      <View style={styles.filterListContainer}>
        <View style={styles.filterButtonsContainer}>
          <TouchableOpacity
            style={styles.filterButton(buttonsHighlight[0])}
            onPress={() => {
              setRenderPurchases(purchases);
              setRefreshList(!refreshList);
              setButtonsHighlights([true, false, false]);
            }}>
            <Text style={styles.filterButtonText}>
              Wszystkie
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.filterButton(buttonsHighlight[1])}
            onPress={() => {
              setRenderPurchases(purchases.filter(getAcceptedOnly));
              setRefreshList(!refreshList);
              setButtonsHighlights([false, true, false]);
            }}>
            <Text style={styles.filterButtonText}>
              Zakończone
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.filterButton(buttonsHighlight[2])}
            onPress={() => {
              setRenderPurchases(purchases.filter(getRejectedOnly));
              setRefreshList(!refreshList);
              setButtonsHighlights([false, false, true]);
            }}>
            <Text style={styles.filterButtonText}>
              Anulowane
            </Text>
          </TouchableOpacity>
        </View>
        { renderPurchases && renderPurchases.length > 0 &&
        <FlatList
          data={renderPurchases}
          renderItem={({ item }) => <PurchaseHistoryRow key={item.id} purchase={item} buttonsHighlight={buttonsHighlight} />}
          keyExtractor={(item, index) => String(index)}
          extraData={refreshList}
        />
        }
      </View>
      :
        <View style={{ marginLeft: 15 }}>
          <Text style={styles.noTokensText}>Przykro nam, nie masz żadnych transakcji.</Text>
        </View>
      }

    </View>
  );
};

export default PurchaseHistoryListView;
