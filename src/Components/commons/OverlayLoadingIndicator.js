import React from 'react'
import { ActivityIndicator, Text, View } from 'react-native'

const OverlayLoadingIndicator = ({ loadingText = 'Trwa ładowanie...' }) => {

  return (
    <View style={{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'}}>
      <ActivityIndicator size="large" color="#0E7AF5"/>
      <Text>{loadingText}</Text>
    </View>
  )
};
export default OverlayLoadingIndicator


