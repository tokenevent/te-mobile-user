import React, { Component } from 'react';
import { FlatList } from 'react-native';
import EventBox from '../../EventBox/EventBox';

export default class MainEventsView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <FlatList
        data={this.props.events}
        renderItem={({ item }) => <EventBox eventData={item} navigation={this.props.navigation}/>}
        keyExtractor={({ id }) => id}
      />
    );
  }
}
