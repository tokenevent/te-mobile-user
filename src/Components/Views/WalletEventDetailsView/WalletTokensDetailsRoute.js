import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import WalletEventDetailsTokensView from "./WalletEventDetailsTokensView";


const WalletTokensDetailsRoute = createStackNavigator({
  WalletTokensDetailsView: {
    screen: WalletEventDetailsTokensView,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default WalletTokensDetailsRoute;
