import React, { Component } from 'react';
import { FlatList, Text, TouchableOpacity, View } from 'react-native';
import { groupBy } from '../../../Utils/util-functions';
import WalletTokensBox from "../../WalletTokensBox/WalletTokensBox";
import globalStylesheet from "../../../Utils/globalStylesheet";
import styles from './WalletEventDetailsTokensView.style';
import FloatingButton from "../../FloatingButton/FloatingButton";

export default class WalletEventDetailsTokensView extends Component {
  constructor(props) {
    super(props);
  }

  prepareTokens() {
    const eventTokens = this.props.navigation.getParam('eventTokens');

    const groupedTokens = groupBy(eventTokens, 'buyableTokenName');
    return (
      <FlatList
        data={Object.keys(groupedTokens)}
        renderItem={({ item }) => <WalletTokensBox tokensName={item} tokensInfo={groupedTokens[item]}/>}
        keyExtractor={(tokenName) => tokenName}
      />
    )
  }

  render() {
    const eventName = this.props.navigation.getParam('eventName', '');
    return (
      <View style={globalStylesheet.fullFlex}>
        <View style={globalStylesheet.viewTitleWrapper}>
          <Text style={globalStylesheet.viewTitle}>Szczegóły żetonów</Text>
          <Text style={globalStylesheet.viewDescription}>{eventName}</Text>
        </View>
        <View style={globalStylesheet.fullFlex}>
          {this.prepareTokens()}
        </View>
        <FloatingButton withBottomButton={true} navigation={this.props.navigation}/>
        <TouchableOpacity
          navigation={this.props.navigation}
          style={styles.blueButton}
          onPress={() => this.props.navigation.navigate('WalletView')}>
          <Text style={globalStylesheet.normalBlueButtonText}>
            Wróć
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
