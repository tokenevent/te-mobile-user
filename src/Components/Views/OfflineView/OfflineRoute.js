import { createStackNavigator } from 'react-navigation-stack';
import OfflineView from './OfflineView';

const OfflineRoute = createStackNavigator({
  OfflineView: {
    screen: OfflineView,
    navigationOptions: {
      header: null,
    },
  },
});
export default OfflineRoute;
