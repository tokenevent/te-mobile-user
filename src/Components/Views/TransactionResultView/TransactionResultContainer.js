import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';
import TransactionResultView from './TransactionResultView';
import { NavigationEvents } from 'react-navigation';
import globalStylesheet from "../../../Utils/globalStylesheet";

export default class TransactionResultContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }

  render() {
    return (
      <View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onDidFocus={() => this.setState({ isLoading: false })}
        />
        {this.state.isLoading && <ActivityIndicator size="large" color="#ff9811"/>}
        {!this.state.isLoading &&
        <TransactionResultView navigation={this.props.navigation} transactionResult={this.props.transactionResult}/>}
      </View>);
  }
}
