import React, { Component } from 'react';
import { View } from 'react-native';
import styles from './TransactionResultScreen.style';
import TransactionResultContainer from './TransactionResultContainer';

export default class TransactionResultScreen extends Component {

  render() {
    return (
      <View style={styles.container}>
        <TransactionResultContainer
          navigation={this.props.navigation}
          transactionResult={this.props.navigation.getParam('transactionResult')}
        />
      </View>
    );
  }
}


