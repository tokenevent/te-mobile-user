import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import WalletMenuEventsScreen from "./WalletMenuEventsScreen";


const WalletViewRoute = createStackNavigator({
  WalletView: {
    screen: WalletMenuEventsScreen,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default WalletViewRoute;
