import {api} from "../Utils/api-urls"
import {request, authorizeRequest} from "../Utils/requests";

const UserService = {
    loginUser: userData => loginUser(userData),
    registerUser: userData => registerUser(userData),
    getUserData: () => getUserData(),
    updateUserData: userData => updateUserData(userData),
    getWalletInfo: () => getWalletInfo(),
    getWalletId: () => getWalletId()
};

const loginUser = userData => {
    const url = api.LOGIN_USER;
    return request().post(url, userData);
};

const registerUser = userData => {
    const url = api.REGISTER_USER;
    return request().post(url, userData);
}

const getUserData = () => {
    const url = api.GET_USER_INFO;
    return authorizeRequest().then(a => a.get(url));
}

const updateUserData = (userData) => {
    const url = api.UPDATE_USER_INFO;
    return authorizeRequest().then(a => a.put(url, userData));
}

const getWalletInfo = () => {
    const url = api.WALLET_INFO;
    return authorizeRequest().then(a => a.get(url));
}

const getWalletId = () => {
    const url = api.WALLET_ID;
    return authorizeRequest().then(a => a.get(url));
}

export default UserService;
