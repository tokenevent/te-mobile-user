import { Dimensions, StyleSheet } from "react-native";

const WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  full: {
    width: WIDTH,
    height: WIDTH/2
  }
});
export default styles
