import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import TransactionResultScreen from "./TransactionResultScreen";


const TransactionResultRoute = createStackNavigator({
  TransactionResultView: {
    screen: TransactionResultScreen,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default TransactionResultRoute;
