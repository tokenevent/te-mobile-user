import { StyleSheet } from "react-native";
import globalStylesheet from "../../../Utils/globalStylesheet";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  blueButton: {
    ...globalStylesheet.normalBlueButtonContainer,
    borderRadius: 0
  }
});
export default styles
