import React from 'react'
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import WavyHeader from "../WavyHeader/WavyHeader";
import hamburger from './../../../assets/images/hamburger.png';

export default class MenuButton extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  render() {
    return (
      <View>
        <View
        style={styles.container}>
        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
          <Image
            ref={this.myRef}
            style={{position: 'absolute', top: 10, left: 40, height: 32, width: 32, zIndex: 99999 }}
            source={hamburger}
            resizeMode='stretch'/>
        </TouchableOpacity>
      </View><WavyHeader/></View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 10,
    left: 13,
    zIndex: 2,
  },
});
