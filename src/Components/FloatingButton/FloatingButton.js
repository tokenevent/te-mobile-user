import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import styles from './FloatingButton.style';
import walletIco from './../../../assets/images/walletIco.png';

const FloatingButton = ({ withBottomButton = false, navigation }) => {
  return <TouchableOpacity style={styles.button(withBottomButton)}
                           onPress={() => navigation.navigate('WalletCodeView')}>
    <Image style={styles.menuPosition}
           source={walletIco}
           resizeMode='stretch'/>
  </TouchableOpacity>
};
export default FloatingButton;
