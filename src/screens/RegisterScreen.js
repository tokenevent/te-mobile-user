import React, { Component } from 'react';
import { Input } from 'react-native-elements';
import { ActivityIndicator, Image, StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import logoWhiteMedium from '../../assets/images/logoWhiteMedium.png';
import UserService from "./../Services/UserService";
import globalStylesheet from './../Utils/globalStylesheet';
import { ScrollView } from 'react-native-gesture-handler';

export default class RegisterScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      email: "",
      phone: "",
      firstName: "",
      lastName: "",
      promoCode: "",
      isLoading: false,
      invalidEmail: false,
      emptyFields: [false, false, false, false, false, false, false]
    }
  }

  onRegister = () => {
    const userData = {
      username: this.state.username,
      password: this.state.password,
      email: this.state.email,
      phone: this.state.phone,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      promoCode: this.state.promoCode
    };

    if (userData.promoCode === "") {
      userData.promoCode = null;
    }

    if (this.validateData(userData)) {
      UserService.registerUser(userData)
        .then(response => this.registerUserSuccessHandler(response))
        .catch(error => this.registerUserErrorHandler(error));
    }
  };

  registerUserSuccessHandler = async response => {
    ToastAndroid.show("Konto utworzone! Możesz zalogować się", ToastAndroid.LONG);
    this.props.navigation.navigate('LoginView');
  };

  registerUserErrorHandler = error => {
    this.setState({ isLoading: false });
    if (error.response.data !== null) {
      if (error.response.data.message === "Promo code does not exist or is inactive.") {
        ToastAndroid.show("Błędny kod promocyjny!", ToastAndroid.SHORT);
      }
    }
    else {
      ToastAndroid.show("Wystąpił błąd podczas rejestracji", ToastAndroid.SHORT);
    }
    console.log(error);
  };

  validateData = (userData) => {
    let result = true;
    let emptyFields = [];
    Object.keys(userData).forEach(data => {
      if (userData[data] === "") {
        result = false;
        emptyFields.push(true);
      } else {
        emptyFields.push(false);
      }
    });

    this.setState({
      emptyFields: emptyFields
    })

    if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(userData.email)) {
      result = false;
      ToastAndroid.show("Nieprawidłowy adres email", ToastAndroid.SHORT);
    }

    return result;
  };

  render() {
    const { isLoading, emptyFields } = this.state;
    return (
      <ScrollView style={styles.scroller}>
        {!isLoading ?
          <View>
            <Image
              style={styles.logo}
              source={logoWhiteMedium}
              resizeMode="contain"
            />
            <Input
              inputStyle={styles.input}
              inputContainerStyle={styles.removeBottomBorder}
              labelStyle={styles.whiteText}
              placeholderTextColor="#77b3fb"
              placeholder="Login"
              textContentType="nickname"
              onChangeText={username => this.setState({ username: username })}
              maxLength={16}
              errorStyle={{ color: 'red' }}
              errorMessage={emptyFields[0] ? 'Pole wymagane' : ''}
            />
            <Input
              inputStyle={styles.input}
              inputContainerStyle={styles.removeBottomBorder}
              labelStyle={styles.whiteText}
              secureTextEntry={true}
              placeholderTextColor="#77b3fb"
              placeholder="Hasło"
              textContentType="password"
              onChangeText={password => this.setState({ password: password })}
              maxLength={20}
              errorStyle={{ color: 'red' }}
              errorMessage={emptyFields[1] ? 'Pole wymagane' : ''}
            />
            <Input
              inputStyle={styles.input}
              inputContainerStyle={styles.removeBottomBorder}
              labelStyle={styles.whiteText}
              placeholderTextColor="#77b3fb"
              placeholder="Email"
              textContentType="emailAddress"
              onChangeText={email => this.setState({ email: email })}
              maxLength={60}
              errorStyle={{ color: 'red' }}
              errorMessage={emptyFields[2] ? 'Pole wymagane' : ''}
            />
            <Input
              inputStyle={styles.input}
              inputContainerStyle={styles.removeBottomBorder}
              labelStyle={styles.whiteText}
              placeholderTextColor="#77b3fb"
              placeholder="Numer telefonu"
              textContentType="telephoneNumber"
              onChangeText={phone => this.setState({ phone: phone })}
              maxLength={20}
              errorStyle={{ color: 'red' }}
              errorMessage={emptyFields[3] ? 'Pole wymagane' : ''}
            />
            <Input
              inputStyle={styles.input}
              inputContainerStyle={styles.removeBottomBorder}
              labelStyle={styles.whiteText}
              placeholderTextColor="#77b3fb"
              placeholder="Imię"
              textContentType="name"
              onChangeText={firstName => this.setState({ firstName: firstName })}
              maxLength={30}
              errorStyle={{ color: 'red' }}
              errorMessage={emptyFields[4] ? 'Pole wymagane' : ''}
            />
            <Input
              inputStyle={styles.input}
              inputContainerStyle={styles.removeBottomBorder}
              labelStyle={styles.whiteText}
              placeholderTextColor="#77b3fb"
              placeholder="Nazwisko"
              textContentType="familyName"
              onChangeText={lastName => this.setState({ lastName: lastName })}
              maxLength={30}
              errorStyle={{ color: 'red' }}
              errorMessage={emptyFields[5] ? 'Pole wymagane' : ''}
            />
            <Input
              inputStyle={styles.input}
              inputContainerStyle={styles.removeBottomBorder}
              labelStyle={styles.whiteText}
              placeholderTextColor="#77b3fb"
              placeholder="Kod promocyjny"
              onChangeText={promoCode => this.setState({ promoCode: promoCode })}
              maxLength={30}
            />
            <TouchableOpacity onPress={this.onRegister}>
              <View style={globalStylesheet.yellowButtonContainer}>
                <Text style={globalStylesheet.yellowButtonText}>ZAREJESTRUJ</Text>
              </View>
            </TouchableOpacity>
          </View>
          :
          <View>
            <ActivityIndicator size="large" color="#ff9811"/>
          </View>}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...globalStylesheet.container,
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#084da0',
  },
  scroller: {
    flex: 1,
    backgroundColor: '#024492',
  },
  input: {
    color: 'white',
    backgroundColor: '#1463c1',
    paddingLeft: 10,
    borderRadius: 5,
    margin: 5,
    fontSize: 14
  },
  removeBottomBorder: {
    borderBottomWidth: 0
  },
  logo: {
    width: undefined,
    height: 80,
    margin: 15
  },
  whiteText: {

    color: "#fff"
  }
});
