import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import noConnectionImg from '../../../../assets/images/no-connection.png';

export default class OfflineView extends Component {

  unsubscribe = NetInfo.addEventListener(state => {
    if (state.isConnected) {
      this.props.navigation.navigate("MainView");
    }
  });

  render() {
    return (
      <View style={{
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#252525"
      }}>
        <Image
          source={noConnectionImg}
          style={{
            alignSelf: 'center',
            width: 100,
            height: 100,
            margin: 30
          }}
        />
        <Text style={{
          fontSize: 30,
          color: "#E74C3C",
          textAlign: 'center',
          margin: 10,
        }}>
          Brak połączenia internetowego
        </Text>
      </View>
    );
  }
}
