import React, { Component } from 'react';
import { ToastAndroid, View } from 'react-native';
import MainEventsView from './MainEventsView';
import EventsService from '../../../Services/EventsService';
import globalStylesheet from '../../../Utils/globalStylesheet';
import { NavigationEvents } from 'react-navigation';
import FloatingButton from "../../FloatingButton/FloatingButton";
import LoadingIndicator from "../../commons/LoadingIndicator";
import logout from "../../../Utils/logout";

export default class MainEventsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      isLoading: true
    };
  }

  prepareData() {
    EventsService.getAllEvents()
      .then(response => this.eventsLoadSuccessHandler(response))
      .catch(error => this.eventsLoadErrorHandler(error));
  }

  eventsLoadSuccessHandler = response => {
    this.setState({
      events: response.data,
      isLoading: false
    })
  };

  eventsLoadErrorHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas pobierania danych wydarzeń!", ToastAndroid.SHORT);
    if (error.response.status === 401) {
      ToastAndroid.show('Wystąpił błąd podczas pobierania danych użytkownika...', ToastAndroid.SHORT);
      this.setState({ isLoading: false });
      logout(this.props.navigation);
    }
  };

  onScreenLoad = () => {
    this.setState({ isLoading: true });
    this.prepareData();

  };

  render() {
    return (
      <View style={globalStylesheet.container}>
        <NavigationEvents
          onWillFocus={this.onScreenLoad}
        />
        {this.state.isLoading && <LoadingIndicator/>}
        {!this.state.isLoading &&
        <>
          <MainEventsView
            navigation={this.props.navigation}
            events={this.state.events}
          />
          <FloatingButton navigation={this.props.navigation}/>
        </>}
      </View>);
  }
}
