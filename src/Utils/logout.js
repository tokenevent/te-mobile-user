import AsyncStorage from '@react-native-community/async-storage';
import { ToastAndroid } from "react-native";

const logout = navigation => {
  AsyncStorage.setItem('accessToken', undefined)
    .then(r => navigation.navigate('AuthLoading'))
    .catch(e => ToastAndroid.show("Wystąpił błąd podczas procesu wylogowania", ToastAndroid.SHORT))
};
export default logout;
