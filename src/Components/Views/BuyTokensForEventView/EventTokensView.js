import React, { Component } from 'react';
import { ActivityIndicator, FlatList, ScrollView, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import globalStylesheet from "../../../Utils/globalStylesheet";
import TokenBox from "../../TokenBox/TokenBox";
import styles from './EventTokensView.style';

const MAX_TOKEN_BUY_AMOUNT = 500;

export default class EventTokensView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tokens: [],
      isLoading: true,
      processingRequest: false
    }
  }

  prepareTokens(tokens) {
    let tokenList = tokens.map(token => {
      return {
        id: token.id,
        name: token.name,
        unitPrice: token.unitPrice,
        quantity: 0
      }
    });
    this.setState({
      tokens: tokenList,
      isLoading: false
    });
  }

  changeTokenAmount = (i, delta) => {
    const tempTokens = [...this.state.tokens];
    let tempResult = tempTokens[i].quantity + delta;
    if (tempResult < 0) {
      tempResult = 0;
    }
    if (tempResult >= 0 && tempResult <= MAX_TOKEN_BUY_AMOUNT) {
      tempTokens[i].quantity = tempResult;
      this.setState({
        tokens: [...tempTokens]
      });
    }
  };

  buyTokens = (tokens) => {
    this.setState({
      processingRequest: true
    });
    let ableToBuy = false;
    let tokensToBuy = tokens.filter(token => {
      if (token.quantity > 0) {
        ableToBuy = true;
        return token;
      }
    });

    if (ableToBuy) {
      this.props.sendTokenData(tokensToBuy);
    } else {
      ToastAndroid.show("Nie wprowadzono żadnej, bądź wpisano nieprawidłową liczbę żetonów!", ToastAndroid.SHORT);
      this.setState({
        processingRequest: false
      });
    }
  };

  getTotalPrice = priceObj => (priceObj.reduce((a, b) => +a + (+b.unitPrice * +b.quantity), 0) / 100);

  render() {
    const { isLoading, tokens } = this.state;
    const { eventName } = this.props;
    return (
      <View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onWillFocus={() => this.prepareTokens(this.props.tokensData)}
        />
        <ScrollView style={globalStylesheet.fullFlex}>
          {isLoading && <ActivityIndicator size="large" color="#ff9811"/>}
          {!isLoading &&
          <View style={globalStylesheet.fullFlex}>
            <View style={styles.headerWrapper}>
              <Text style={globalStylesheet.viewTitle}>
                Kup żetony
              </Text>
              <Text style={globalStylesheet.viewDescription}>
                {eventName}
              </Text>
            </View>
            <FlatList
              data={this.state.tokens}
              renderItem={({ item, index }) =>
                <TokenBox
                  tokenData={item}
                  i={index}
                  navigation={this.props.navigation}
                  changeTokenAmount={this.changeTokenAmount}
                />
              }
              keyExtractor={({ id }) => id}
            />
          </View>
          }
        </ScrollView>

        <View style={globalStylesheet.borderTop}>
          <View style={globalStylesheet.summaryWrapper}>
            <Text style={globalStylesheet.summaryText}>Łączny koszt: </Text>
            <Text style={globalStylesheet.summaryAmount}>{this.getTotalPrice(tokens)}</Text>
            <Text style={globalStylesheet.summaryCurrency}>zł</Text>
          </View>
          <TouchableOpacity
            style={styles.blueButton(this.getTotalPrice(tokens) === 0)}
            disabled={this.getTotalPrice(tokens) === 0}
            onPress={() => !this.state.processingRequest && this.buyTokens(this.state.tokens)}>
            <Text style={globalStylesheet.normalBlueButtonText}>
              Kup
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
