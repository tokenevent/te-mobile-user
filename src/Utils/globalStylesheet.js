import { Dimensions, StyleSheet } from 'react-native';

const WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  yellowButtonContainer: {
    fontWeight: 'bold',
    height: 60,
    marginTop: 5,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#ff9811',
    borderRadius: 5,
    justifyContent: 'center',
    textAlign: 'center'
  },
  yellowButtonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700'
  },
  normalTextInput: {
    color: '#fff',
    height: 45,
    paddingLeft: 10,
    borderRadius: 5,
    backgroundColor: '#1463c1',
    margin: 15
  },
  normalBlueButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0E7AF5',
    height: 70,
    width: '100%',
    borderRadius: 10
  },
  normalBlueButtonContainerWithoutRadius: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0E7AF5',
    height: 70,
    width: '100%',
  },
  normalBlueButtonText: {
    color: "#fff",
    fontSize: 27,
    textTransform: 'uppercase',
    fontFamily: 'TwCenMT-Condensed'
  },
  eventContainer: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 10,
    width: WIDTH - 50
  },
  eventTitle: {
    fontFamily: 'TwCenMT',
    color: '#0b3a66',
    fontSize: 46,
    margin: 5,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  eventDate: {
    color: '#000000',
    fontSize: 15,
    textAlign: 'left',
    display: 'flex',
    flexDirection: 'column'
  },
  eventAddress: {
    color: '#000000',
    fontSize: 15,
    textAlign: 'left'
  },
  whiteDetailsButton: {
    alignItems: 'center',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    width: '75%'
  },
  detailsButton: {
    alignItems: 'center',
    backgroundColor: '#1b5b96',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    width: '75%'
  },
  eventImage: {
    alignSelf: 'center',
    width: '100%',
    height: '100%',
    margin: 1,

  },
  viewTitle: {
    fontFamily: "TwCenMT-Condensed",
    fontSize: 51,
    color: 'black'
  },
  viewDescription: {
    fontFamily: "TwCenMT-Condensed",
    fontSize: 28,
  },
  tokenName: {
    textTransform: 'uppercase',
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 31
  },
  slash: {
    fontFamily: 'TwCenMT',
    fontSize: 14,
    fontWeight: '800',
    color: '#C8C8C8',
    marginLeft: 10,
    marginRight: 10
  },
  unitPrice: {
    fontFamily: 'TwCenMT',
    fontSize: 18,
    fontWeight: '800',
    color: 'black'
  },
  ordered: {
    fontFamily: 'TwCenMT-Condensed',
    textTransform: 'uppercase',
    fontSize: 18
  },
  orderedAmount: {
    fontFamily: 'TwCenMT-Condensed',
    color: 'black',
    fontSize: 18,
    marginRight: 3
  },
  szt: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 18
  },
  availableTokens: {
    fontFamily: 'TwCenMT',
    fontSize: 32,
    fontWeight: '800',
    color: 'black'
  },
  availableQty: {
    fontFamily: "TwCenMT-Condensed",
    fontSize: 32,
  },
  borderTop: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: '#707070',
  },
  summaryWrapper: {
    display: 'flex',
    flexDirection: 'row',
    margin: 10
  },
  summaryText: {
    fontFamily: 'TwCenMT',
    fontWeight: '800',
    textTransform: 'uppercase',
    fontSize: 18,
  },
  summaryAmount: {
    fontFamily: 'TwCenMT',
    fontWeight: '800',
    color: 'black',
    textTransform: 'uppercase',
    fontSize: 18,
    marginRight: 5
  },
  summaryCurrency: {
    fontFamily: 'TwCenMT',
    fontWeight: '800',
    fontSize: 18,
  },
  fullFlex: {
    flex: 1,
  },
  viewTitleWrapper: {
    marginLeft: 15,
    marginBottom: 10,
    marginTop: 10
  }

});
