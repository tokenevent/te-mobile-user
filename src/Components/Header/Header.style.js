import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 10,
    left: 13,
    zIndex: 2,
  },
});
export default styles;
