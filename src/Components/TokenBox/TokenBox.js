import React, { Component } from 'react';
import { Text, View } from 'react-native';
import styles from './TokenBox.style';
import BuyTokenButton from "../BuyTokenButton/BuyTokenButton";
import globalStylesheet from "../../Utils/globalStylesheet";


export default class TokenBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isValid: true
    }
  }

  render() {
    const { tokenData, changeTokenAmount, i: index } = this.props;

    return (
      <View style={styles.mainWrapper}>
        <View style={styles.contentWrapper}>
          <View style={styles.boxWrapper}>
            <View style={styles.rowWrapper}>
              <Text style={globalStylesheet.tokenName}>{tokenData.name}</Text>
              <Text style={globalStylesheet.slash}>/</Text>
              <Text style={globalStylesheet.unitPrice}>{tokenData.unitPrice / 100}zł</Text>
            </View>
            <View style={styles.rowWrapper}>
              <Text style={globalStylesheet.ordered}>Dostępne:</Text>
              <Text style={styles.availableAmount}>1000</Text>
              <Text style={globalStylesheet.szt}>szt.</Text>
            </View>
          </View>
          <View style={styles.boxWrapper}>
            <View style={styles.rowWrapper}>
              <Text style={globalStylesheet.ordered}>Zamówione: </Text>
              <Text style={globalStylesheet.orderedAmount}>{tokenData.quantity}</Text>
              <Text style={globalStylesheet.szt}>szt.</Text>
            </View>
            <View style={styles.rowWrapper}>
              <Text style={globalStylesheet.ordered}>Koszt:</Text>
              <Text style={styles.availableAmount}>{tokenData.quantity * (tokenData.unitPrice / 100)}</Text>
              <Text style={globalStylesheet.szt}>zł</Text>
            </View>
          </View>
          <View style={styles.buttonsWrapper}>
            <BuyTokenButton borderColor='#EC4C4C' text="-1" index={index} onClick={changeTokenAmount}/>
            <BuyTokenButton borderColor='#55AC40' text="+1" index={index} onClick={changeTokenAmount}/>
          </View>
          <View style={styles.buttonsWrapper}>
            <BuyTokenButton borderColor='#EC4C4C' text="-3" index={index} onClick={changeTokenAmount}/>
            <BuyTokenButton borderColor='#55AC40' text="+3" index={index} onClick={changeTokenAmount}/>
          </View>
        </View>
      </View>
    )
  }
}
