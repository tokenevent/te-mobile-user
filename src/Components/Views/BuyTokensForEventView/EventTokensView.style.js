import { StyleSheet } from "react-native";
import globalStylesheet from "../../../Utils/globalStylesheet";

const styles = StyleSheet.create({
  container: {
    margin: 15,
    marginBottom: 60,
  },
  buyButton: {
    alignItems: 'center',
    backgroundColor: '#1b5b96',
    color: 'white',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    width: '75%'
  },
  blueButton: (disabled) => {
    return {
      ...globalStylesheet.normalBlueButtonContainer,
      borderRadius: 0,
      opacity: disabled ? 0.5 : 1
    }
  },
  headerWrapper: {
    marginLeft: 15
  }

});
export default styles
