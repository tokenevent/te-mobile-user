import React from 'react';
import UserService from '../../../Services/UserService';
import SettingsView from './SettingsView';
import { ToastAndroid, View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import LoadingIndicator from '../../commons/LoadingIndicator';
import AsyncStorage from '@react-native-community/async-storage';
import globalStylesheet from "../../../Utils/globalStylesheet";

export default class SettingsContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      userData: []
    };
  }

  getUserData = () => {
    UserService.getUserData()
      .then(response => this.userDataSuccessHandler(response))
      .catch(error => this.userDataErorrHandler(error));
  };

  userDataSuccessHandler = response => {
    this.setState({
      userData: response.data.user.account,
      isLoading: false
    });
  };

  userDataErorrHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas pobierania ustawień", ToastAndroid.SHORT);
  };

  updateUserData = (userData) => {
    UserService.updateUserData(userData)
      .then(response => this.updateUserSuccesHandler(response))
      .catch(error => this.updateUserErrorHandler(error));
  };

  updateUserSuccesHandler = async response => {
    ToastAndroid.show("Aktualizacja danych użytkownika przebiegła pomyślnie!", ToastAndroid.SHORT);
    const acc = response.data.account;
    AsyncStorage.multiSet([['name', acc.firstName + " " + acc.lastName], ['email', acc.email]])
      .then(r => this.props.navigation.navigate("MainView"))
      .catch(e => console.log(e, 'error'))
  };

  updateUserErrorHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas aktualizowania ustawień. Prosimy spróbować ponownie!", ToastAndroid.SHORT);
  };

  render() {
    const { userData, isLoading } = this.state;
    return (
      <View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onWillFocus={() => {
            this.setState({
              userData: [],
              isLoading: true
            });
            this.getUserData();
          }
          }
        />
        {isLoading && <LoadingIndicator loadingText="Ładowanie ustawień"/>}
        {!isLoading && <SettingsView userData={userData} updateData={this.updateUserData}/>}
      </View>);
  }
}


