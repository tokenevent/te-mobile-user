import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import EventDetailsContainer from "./EventDetailsContainer";


const EventDetailsRoute = createStackNavigator({
  EventDetailsView: {
    screen: EventDetailsContainer,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default EventDetailsRoute;
