import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import MainEventsContainer from "./MainEventsContainer";


const MainViewRoute = createStackNavigator({
  MainView: {
    screen: MainEventsContainer,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default MainViewRoute;
