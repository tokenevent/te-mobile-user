import { Dimensions, StyleSheet } from "react-native";
import globalStylesheet from "../../Utils/globalStylesheet";


const WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    display: 'flex',
    backgroundColor: '#fff',
    flexDirection: 'column',
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 0,
    elevation: 6,
    width: WIDTH - 50
  },
  eventTitle: {
    color: '#505050',
    fontSize: 45,
    fontFamily: 'TwCenMT-Condensed',
  },
  detailsTextHeader: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 17,
    textAlign: 'left',
    textTransform: 'uppercase',
    justifyContent: 'flex-start',
    color: '#707070'
  },
  detailsText: {
    color: '#000000',
    fontSize: 15,
    fontFamily: 'TwCenMT',
    fontWeight: 'bold'
  },
  detailsWrapper: {
    marginTop: 15,
    width: 269,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    textAlign: 'left',
    flexDirection: 'column',
    padding: 5
  },
  detailsTextHeaderWrapper: {
    width: '100%',
    display: 'flex',
    textAlign: 'left',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    alignContent: 'flex-start'
  },
  dateWrapper: {
    display: 'flex',
    flexDirection: 'column'
  },
  detailsButtonContainer: {
    width: '100%',
    padding: 15
  },
  imageWrapper: {
    borderWidth: 0,
    borderRadius: 20,
    borderColor: '#707070',
    width: 269,
    height: 269,
    marginTop: 5,
    overflow: 'hidden'
  },
  detailsButtonText: {
    ...globalStylesheet.normalBlueButtonText,
    fontFamily: 'TwCenMT',
    fontSize: 17
  }

});
