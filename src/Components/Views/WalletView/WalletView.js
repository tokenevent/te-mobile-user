import React, { Component } from 'react';
import { FlatList, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import WalletEventsBox from '../../WalletEventsBox/WalletEventsBox';
import globalStylesheet from "../../../Utils/globalStylesheet";
import styles from './WalletView.style';

export default class WalletView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={globalStylesheet.fullFlex}>
          <View style={globalStylesheet.viewTitleWrapper}>
            <Text style={globalStylesheet.viewTitle}>Portfel</Text>
          </View>
          <FlatList
            data={this.props.walletInfo}
            renderItem={({ item }) => <WalletEventsBox navigation={this.props.navigation} walletInfo={item}/>}
            keyExtractor={({ event }) => event.id}
          />
        </ScrollView>
        <TouchableOpacity
          navigation={this.props.navigation}
          style={styles.blueButton}
          onPress={() => this.props.navigation.navigate('WalletCodeView')}>
          <Text style={globalStylesheet.normalBlueButtonText}>
            Pokaż kod portfela
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}


