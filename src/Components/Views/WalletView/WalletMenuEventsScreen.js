import React, { Component } from 'react';
import { ToastAndroid, View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import UserService from '../../../Services/UserService';
import WalletView from './WalletView';
import globalStylesheet from '../../../Utils/globalStylesheet';
import LoadingIndicator from "../../commons/LoadingIndicator";

export default class WalletMenuEventsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      walletInfo: [],
      isLoading: true
    };
  }

  prepareData() {
    UserService.getWalletInfo()
      .then(response => this.walletInfoLoadSuccessHandler(response))
      .catch(error => this.walletInfoLoadErrorHandler(error));
  }

  walletInfoLoadSuccessHandler = response => {
    this.setState({
      walletInfo: response.data.events,
      isLoading: false
    })
  };

  walletInfoLoadErrorHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas pobierania informacji portfela!", ToastAndroid.SHORT);
    this.setState({
      isLoading: false
    })
  };

  render() {
    const { isLoading } = this.state;
    return (
      <View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onWillFocus={() => {
            this.setState({ isLoading: true });
            this.prepareData();
          }}
        />
        {isLoading && <LoadingIndicator/>}
        {!isLoading && <WalletView
          navigation={this.props.navigation}
          walletInfo={this.state.walletInfo}
        />}
      </View>
    );
  }
}
