import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import BuyTokensForEventView from "./BuyTokensForEventView";


const BuyTokensView = createStackNavigator({
  BuyTokensView: {
    screen: BuyTokensForEventView,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default BuyTokensView;
