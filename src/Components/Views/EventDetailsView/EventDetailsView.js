import React, { Component } from 'react';
import { Image, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import globalStylesheet from "../../../Utils/globalStylesheet";
import style from './EventDetailsView.style';
import j_placeholder from '../../../../assets/images/juwe_placeholder.png';
import EventMap from "../../EventMap/EventMap";

export default class EventDetailsView extends Component {
  constructor(props) {
    super(props);
  }

  onBuyTokensClick = () => {
    const { eventData } = this.props;
    if (eventData.buyableTokens && eventData.buyableTokens.length > 0) {
      this.props.navigation.navigate('BuyTokensView', {
        tokens: this.props.eventData.buyableTokens,
        eventId: this.props.eventData.id,
        eventName: this.props.eventData.name
      });
    } else {
      ToastAndroid.show("Aktualnie nie ma dostępnych żetonów. Spróbuj ponownie później...", ToastAndroid.SHORT);
    }
  };

  getParsedDate = date => {
    const parsed = new Date(date);
    return `${parsed.toLocaleDateString()} (${parsed.toLocaleTimeString()})`;
  };

  render() {
    const { eventData } = this.props;

    return (
      <View style={globalStylesheet.fullFlex}>
        <ScrollView>
          <View style={style.imageWrapper}>
            <Image
              style={globalStylesheet.eventImage}
              source={j_placeholder}
              resizeMode="contain"
            />
          </View>
          <View style={style.textContainer}>
            <Text style={style.detailsTextHeader}>Opis</Text>
            <Text style={style.detailsText}>{eventData.description}</Text>
            <Text style={style.detailsTextHeader}>Termin</Text>
            <Text style={style.detailsText}>{this.getParsedDate(eventData.startingAt)}</Text>
            <Text style={style.detailsText}>{this.getParsedDate(eventData.endingAt)}</Text>
            <Text style={style.detailsTextHeader}>Miejsce</Text>
            <Text
              style={style.detailsText}>{eventData.address.city} ({eventData.address.zipCode}), {eventData.address.street}</Text>

          </View>
          <EventMap
            eventName={eventData.eventName}
            lat={eventData.address.coordinates.lat}
            lon={eventData.address.coordinates.lon}
          />
        </ScrollView>
        { this.props.eventData.id !== 'c0a9ee5c-57df-459d-b980-7375e64358bb'}
        <View>
          <TouchableOpacity
            navigation={this.props.navigation}
            onPress={this.onBuyTokensClick}
            style={style.buyTokensButton}
          >
            <Text style={style.buyTokensText}>Kup żetony</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
