import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import TransactionHistoryView from "./TransactionHistoryView";


const TransactionHistoryRoute = createStackNavigator({
  TransactionHistoryView: {
    screen: TransactionHistoryView,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default TransactionHistoryRoute;
