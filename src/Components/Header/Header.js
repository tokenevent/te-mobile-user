import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import WavyHeader from '../WavyHeader/WavyHeader';
import styles from './Header.style';

const Header = ({ navigation }) => {
  return (<View><View
    style={styles.container}>
    <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
      <Image
        style={{ height: 32, width: 32 }}
        source={require('./../../../assets/images/hamburger.png')}
        resizeMode='stretch'/>
    </TouchableOpacity>
  </View><WavyHeader/></View>);
};
export default Header;
