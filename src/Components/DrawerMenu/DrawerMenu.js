import React from 'react';
import { Image, ScrollView, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import { DrawerNavigatorItems, TouchableItem } from 'react-navigation-drawer';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './DrawerMenu.style';
import NetInfo from "@react-native-community/netinfo";

const DrawerMenu = ({ navigation, descriptors, ...props }) => {
  const [name, onNameChange] = React.useState('');
  const [email, onEmailChange] = React.useState('');

  const navLink = React.useCallback((onClickFunction, text, icon) => {
    return (
      <TouchableOpacity style={{ height: 50 }} onPress={onClickFunction}>
        <View style={{ marginLeft: 18 }}>
          <View style={styles.menuPositionContainer}>
            <Image style={styles.menuPosition}
                   source={icon}
                   resizeMode='stretch'/>
            <Text style={styles.link}>{text}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }, []);

  const unsubscribe = NetInfo.addEventListener(state => {
    if (!state.isConnected) {
      navigation.navigate('OfflineView');
    }
  });

  const signOut = async () => {
    await AsyncStorage.clear();
    navigation.navigate('Auth');
  };

  const saveSettingsSuccessHandler = response => {
    onNameChange(response[0][1]);
    onEmailChange(response[1][1]);
  };

  const saveSettingsErrorHandler = e => {
    ToastAndroid.show("Wystąpił błąd podczas zapisu ustawień...", ToastAndroid.SHORT);
  };

  const getData = () => {
    AsyncStorage.multiGet(['name', 'email'])
      .then(response => saveSettingsSuccessHandler(response))
      .catch(e => saveSettingsErrorHandler(e));
  };
  return (
    <View style={styles.container}>
      <ScrollView style={styles.scroller}>
        <LinearGradient style={styles.gradient} colors={['#024492', '#148EC1']} useAngle={true} angle={45}
                        angleCenter={{ x: 0.5, y: 0.5 }}>
          <View style={styles.topLinks}>
            <View style={styles.profile}>
              <View style={styles.imgView}>
                <Image style={styles.img} source={require('../../../assets/images/placeholder.jpg')}/>
              </View>
              <View style={styles.profileText}>
                <Text style={styles.name}>{name}</Text>
                <View style={{ alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.city}>{email}</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <ScrollView style={styles.bottomLinks}>
            {getData()}
            <DrawerNavigatorItems
              {...props}
              labelStyle={styles.drawerMenuLink}
            />
            {navLink(signOut, 'WYLOGUJ', require('./../../../assets/images/logoutIco.png'))}
          </ScrollView>
        </LinearGradient>
      </ScrollView>
    </View>
  );
};

export default DrawerMenu;
