import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import PurchaseHistoryView from "./PurchaseHistoryView";


const PurchaseHistoryRoute = createStackNavigator({
  PurchaseHistoryView: {
    screen: PurchaseHistoryView,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default PurchaseHistoryRoute;
