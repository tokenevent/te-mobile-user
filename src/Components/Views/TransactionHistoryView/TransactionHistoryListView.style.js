import { StyleSheet } from "react-native";
import globalStylesheet from "../../../Utils/globalStylesheet";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  filterListContainer: {
    flex: 1,
    margin: 5
  },
  filterButtonsContainer: {
    flexDirection: "row"
  },
  filterButton: isActive => {
    return {
      ...globalStylesheet.normalBlueButtonContainerWithoutRadius,
      width: "33%",
      height: 48,
      backgroundColor: isActive ? "#0E7AF5" : "#2999ff"
    }
  },
  filterButtonText: {
    ...globalStylesheet.normalBlueButtonText,
    fontSize: 18,
    fontFamily: 'TwCenMT-Condensed'
  },
  noTokensText: {
    fontFamily: "TwCenMT-Condensed",
    fontSize: 20,
  }

});

export default styles;
