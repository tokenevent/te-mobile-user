import { StyleSheet } from "react-native";
import React from "react";
import { Input } from 'react-native-elements';
import globalStylesheet from './../../Utils/globalStylesheet';

const SettingsField = ({ placeholder, value, index, onChangeValue }) => {
  const [fieldValue, onChangeText] = React.useState(value);

  return (
      <Input
        value={fieldValue}
        inputStyle={styles.input}
        inputContainerStyle={styles.removeBottomBorder}
        labelStyle={styles.whiteText}
        placeholderTextColor="#77b3fb"
        placeholder={placeholder}
        onChangeText={(text) => {onChangeValue(text, index); onChangeText(text)}}
        maxLength={30}
      />
  );
};

export default SettingsField

const styles = StyleSheet.create({
  container: {
    ...globalStylesheet.container,
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#084da0',
  },
  input: {
    color: 'white',
    backgroundColor: '#1463c1',
    paddingLeft: 10,
    borderRadius: 5,
    margin: 5,
    fontSize: 14
  },
  removeBottomBorder: {
    borderBottomWidth: 0
  },
  logo: {
    width: undefined,
    height: 80,
    margin: 15
  },
  whiteText: {

    color: "#fff"
  }
});
