import { Dimensions, StyleSheet } from "react-native";
import globalStylesheet from "../../../Utils/globalStylesheet";
const WIDTH = Dimensions.get('window').width;
export default StyleSheet.create({
  buyTokensButton: {
    ...globalStylesheet.normalBlueButtonContainer,
    borderRadius: 0,
    backgroundColor: '#FFC31E',
  },
  imageWrapper: {
    width: WIDTH,
    height: WIDTH,
  },
  buyTokensText: {
    ...globalStylesheet.normalBlueButtonText,
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 20
  },
  detailsTextHeader: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 17,
    textAlign: 'left',
    textTransform: 'uppercase',
    justifyContent: 'flex-start',
    color: '#707070'
  },
  detailsText: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 18,
    color: '#1E1E1E'
  },
  textContainer: { margin: 5 }
});
