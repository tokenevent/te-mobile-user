import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import DrawerMenu from "../../DrawerMenu/DrawerMenu";
import Header from "../../Header/Header";
import SettingsContainer from "./SettingsContainer";


const SettingsRoute = createStackNavigator({
  SettingsContainer: {
    screen: SettingsContainer,
    navigationOptions: {
      contentComponent: props => <DrawerMenu {...props} />,
      header: ({ navigation }) => <Header navigation={navigation}/>,
    },
  },
});
export default SettingsRoute;
