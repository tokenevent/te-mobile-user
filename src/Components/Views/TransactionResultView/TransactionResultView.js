import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import failedImage from '../../../../assets/images/close.png';
import successImage from '../../../../assets/images/success.png';
import globalStylesheet from "../../../Utils/globalStylesheet";
import styles from './TransactionResultView.style';

const SUCCESS_HEADER = "Sukces!";
const SUCCESS_DESC = "Transakcja zakończona pomyślnie.\nMożesz odebrać swoje produkty";

const CANCELLED_HEADER = "Anulowano";
const CANCELLED_DESC = "Transakcja została anulowana.";

const NOT_ENOUGH_HEADER = "Brak środków";
const NOT_ENOUGH_DESC = "Niewystarczająca ilość żetonów";

const NO_TRANSACTION_HEADER = "Transakcja anulowana";
const NO_TRANSACTION_DESC = "Sprzedający anulował transackję";

export default class TransactionResultView extends Component {
  constructor(props) {
    super(props);
  }

  onProceed = () => this.props.navigation.navigate('MainView');

  render() {
    const { transactionResult } = this.props;
    return (
      <View style={globalStylesheet.fullFlex}>
        <View style={styles.infoWrapper}>
          <Image
            style={transactionResult === "ACCEPTED" ? styles.successImg : styles.failImg}
            source={transactionResult === "ACCEPTED" ? successImage : failedImage}
            resizeMode="contain"
          />
          <Text style={styles.resultHeader}>
            {transactionResult === "ACCEPTED" && SUCCESS_HEADER }
            {transactionResult === "REJECTED" && CANCELLED_HEADER }
            {transactionResult === "NOT_ENOUGH_TOKENS" && NOT_ENOUGH_HEADER }
            {transactionResult === "NO_TRANSACTION" && NO_TRANSACTION_HEADER }
          </Text>
          <Text style={styles.resultDesc}>
            {transactionResult === "ACCEPTED" && SUCCESS_DESC }
            {transactionResult === "REJECTED" && CANCELLED_DESC }
            {transactionResult === "NOT_ENOUGH_TOKENS" && NOT_ENOUGH_DESC }
            {transactionResult === "NO_TRANSACTION" && NO_TRANSACTION_DESC }
          </Text>
        </View>
        <View>
          <TouchableOpacity
            onPress={this.onProceed}
            style={globalStylesheet.normalBlueButtonContainerWithoutRadius}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>Wróć</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
