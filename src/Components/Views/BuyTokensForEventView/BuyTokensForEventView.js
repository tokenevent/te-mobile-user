import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import { StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from 'react-native';
import TokenService from '../../../Services/TokenService';
import LoadingIndicator from '../../commons/LoadingIndicator';
import PurchaseService from '../../../Services/PurchaseService';
import globalStylesheet from "../../../Utils/globalStylesheet";
import EventTokensView from "./EventTokensView";

export default class BuyTokensForEventView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventId: this.props.navigation.getParam('eventId'),
      eventName: this.props.navigation.getParam('eventName'),
      processing: false,
      payuUrl: '',
      statusPollingInterval: null,
      seconds: 0,
      purchaseMade: false,
      transactionId: '',
    }
  }

  sendTokenData = (tokensToBuy) => {
    const tokensData = { eventId: this.state.eventId, tokensToBuy };

    TokenService.issueTokens(tokensData)
      .then(response => this.tokensSendSuccessHandler(response))
      .catch(error => this.tokensSendErrorHandler(error))
  };

  tokensSendSuccessHandler = response => {
    this.setState({
      processing: true,
      payuUrl: response.data.locationUrl,
      transactionId: response.data.transactionId
    });
    this.setState({
      statusPollingInterval: setInterval(() => this.checkPurchaseStatus(), 15000)
    });
    this.addSeconds();
  };

  tokensSendErrorHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas zakupu żetonów! Prosimy spróbować ponownie", ToastAndroid.SHORT);
    console.log(error);
  };

  checkPurchaseStatus = () => {
    PurchaseService.getPurchaseStatus(this.state.transactionId)
      .then(response => this.checkPurchaseStatusSuccessHandler(response))
      .catch(error => this.checkPurchaseStatusStatusErrorHandler(error));
  };

  checkPurchaseStatusSuccessHandler = response => {
    if (response.data.currentStatus === "COMPLETED") {
      this.props.navigation.navigate('PurchaseResultView', { result: "COMPLETED" });
      this.onRedirect();
    } else if (response.data.currentStatus === "CANCELED") {
      this.props.navigation.navigate('PurchaseResultView', { result: "CANCELED" });
      this.onRedirect();
    }
  };

  checkPurchaseStatusStatusErrorHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas pobierania informacji o zakupie", ToastAndroid.SHORT);
    console.log(error);
  };

  checkForProcessingEnd = (event) => {
    if (event.url.includes('tokenevent')) {
      this.setState({ processing: false, purchaseMade: true });
    }
  };

  addSeconds = () => {
    this.interval = setInterval(() => {
      if (this.state.seconds < 120)
        this.setState({
          seconds: this.state.seconds + 1
        });
      else {
        clearInterval(this.state.statusPollingInterval);
        clearInterval(this.interval);
      }
    }, 1000);
  };

  onProceed = () => {
    this.props.navigation.navigate('MainView');
    this.onRedirect();
  };

  onRedirect = () => {
    clearInterval(this.state.statusPollingInterval);
    this.setState({
      eventId: this.props.navigation.getParam('eventId'),
      processing: false,
      payuUrl: '',
      statusPollingInterval: null,
      seconds: 0,
      purchaseMade: false,
      transactionId: '',
    });
  };


  render() {
    const { eventName } = this.state;

    if (!this.state.processing)
      if (!this.state.purchaseMade)
        return (<View style={globalStylesheet.fullFlex}>
            <EventTokensView
              navigation={this.props.navigation}
              sendTokenData={this.sendTokenData}
              eventName={eventName}
              tokensData={this.props.navigation.getParam('tokens')}
            />
          </View>
        );
      else
        return (
          <View style={styles.container}>
            <View style={globalStylesheet.fullFlex}>
              <LoadingIndicator loadingText={"Oczekiwanie na realizację płatności\n\nJeśli nie chcesz czekać wciśnij pomiń"}/>
            </View>
            <View>
              <TouchableOpacity
                onPress={this.onProceed}
                style={globalStylesheet.normalBlueButtonContainerWithoutRadius}
              >
                <Text style={globalStylesheet.normalBlueButtonText}>Pomiń</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
    else {
      return (
        <WebView
          source={{ uri: this.state.payuUrl }}
          onNavigationStateChange={event => this.checkForProcessingEnd(event)}
        />);
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  proceed: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1b5b96',
    color: 'white',
    height: 64,
    borderRadius: 10,
    padding: 10,
    margin: 80,
    width: '85%'
  }
});
