import React, { useEffect } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { toPlnString } from "../../Utils/util-functions";
import failedImage from '../../../assets/images/close.png';
import successImage from '../../../assets/images/success.png';
import pendingImg from '../../../assets/images/pendingImage.png';
import TransactionService from '../../Services/TransactionsService';
import LoadingIndicator from "../commons/LoadingIndicator";
import TransactionConsentBox from "../TransactionConsentBox/TransactionConsentBox";


const TransactionHistoryRow = ({ transaction, buttonsHighlight }) => {
  const total = transaction.tokens.totalValue;
  const totalString = toPlnString(total);
  const transactionDate = `${new Date(transaction.transactionAt).toLocaleDateString()}, ${new Date(transaction.transactionAt).toLocaleTimeString()}`;
  const [isDetailsExpanded, setExpand] = React.useState(false);
  const [isLoading, setLoading] = React.useState(false);
  const [transactionDetails, setTransactionDetails] = React.useState({});

  useEffect(() => {
    setExpand(false)
  }, [buttonsHighlight]);

  const getDetails = id => {

    if (!(transactionDetails && transactionDetails.scannerName)) {
      setLoading(true);
      TransactionService.getTransactionDetails(id)
        .then(response => {
          setLoading(false);
          setTransactionDetails(response.data);
        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        });
    }
    setExpand(!isDetailsExpanded)
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.transaction}
        onPress={() => {
          getDetails(transaction.id)
        }}>
        <View style={styles.container_token}>
          <Text>Ilość</Text>
          <Text style={{ fontWeight: 'bold' }}>{transaction.tokens.count}</Text>
        </View>

        <View style={styles.container_text}>
          <Text style={styles.title}>
            {transaction.eventName}
          </Text>
          <Text style={styles.description}>
            Wartość zakupów: {totalString}zł{"\n"}
            Data transakcji {transactionDate}{"\n"}
            Zeskanował: <Text style={{fontWeight: 'bold'}}>{transaction.scannerUserName}</Text>
          </Text>
        </View>
        {transaction.status === "ACCEPTED"
        &&
        <View style={styles.container_icon}>
          <Image
            style={styles.successImg}
            source={successImage}
            resizeMode="contain"
          />
          <Text style={styles.successDesc}>
            PRZYJĘTA
          </Text>
        </View>
        }
        {transaction.status === "REJECTED"
        &&
        <View style={styles.container_icon}>
          <Image
            style={styles.failedImg}
            source={failedImage}
            resizeMode="contain"
          />
          <Text style={styles.failDesc}>
            ODRZUCONA
          </Text>
        </View>
        }
        {transaction.status === "PENDING"
        &&
        <View style={styles.container_icon}>
          <Image
            style={styles.pendingImg}
            source={pendingImg}
            resizeMode="contain"
          />
          <Text style={styles.pendingDesc}>
            W TRAKCIE
          </Text>
        </View>
        }
      </TouchableOpacity>
      {isDetailsExpanded && !isLoading && transactionDetails &&
      (transactionDetails.tokens || []).map((t, index) => {
        return <TransactionConsentBox
          key={index}
          amount={t.amount}
          unitPrice={t.unitPrice}
          tokenName={t.buyableTokenName}
        />
      })
      }
      {isLoading && <LoadingIndicator withMargin={false} loadingText="Wczytywanie szczegółów..."/>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#FFF',
    elevation: 2,
  },
  transaction: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF'
  },
  title: {
    fontSize: 20,
    color: '#000',
  },
  container_text: {
    flex: 3,
    paddingLeft: 8
  },
  container_token: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container_icon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  description: {
    fontSize: 11,
    fontStyle: 'italic',
  },
  successDesc: {
    fontSize: 11,
    color: 'green'
  },
  failDesc: {
    fontSize: 10,
    color: 'red'
  },
  successImg: {
    alignSelf: 'center',
    width: 32,
    height: 32,
    margin: 5
  },
  failedImg: {
    alignSelf: 'center',
    width: 32,
    height: 32,
    margin: 5
  },
  pendingDesc: {
    fontSize: 11,
    color: '#888888'
  },
  pendingImg: {
    alignSelf: 'center',
    width: 32,
    height: 32,
    margin: 5
  },
});


export default TransactionHistoryRow;
