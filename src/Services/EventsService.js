import {api} from "../Utils/api-urls"
import {authorizeRequest} from "../Utils/requests";

const EventsService = {
    getAllEvents: () => getAllEvents(),
    getEvent: (id) => getEvent(id)
};
const getAllEvents = () => {
    const url = api.ALL_EVENTS;
    return authorizeRequest().then(a => a.get(url));
};
const getEvent = (id) => {
    const url = api.SINGLE_EVENTS + id;
    return authorizeRequest().then(a => a.get(url));
}

export default EventsService;
