import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import styles from './WalletEventsBox.style';
import globalStylesheet from "../../Utils/globalStylesheet";
import j_placeholder from '../../../assets/images/juwe_placeholder.png';

export default class WalletEventsBox extends Component {
  constructor(props) {
    super(props);
  }

  onPress = () => {
    this.props.navigation.navigate('WalletTokensDetailsView', {
      eventTokens: this.props.walletInfo.tokens,
      eventName: this.props.walletInfo.event.name
    });
  };

  render() {
    const { walletInfo } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.boxContentWrapper}>
          <View style={styles.imageContainer}>
            <Image
              style={globalStylesheet.eventImage}
              source={j_placeholder}
              resizeMode="contain"
            />
          </View>
          <View style={styles.wrapper}>
            <Text style={styles.eventName}>{walletInfo.event.name}</Text>
            <View style={styles.boxContentWrapper}>
              <Text style={styles.boxTextHeader}>Ilość: </Text>
              <Text style={styles.boxTextValue}>{walletInfo.tokens.length}</Text>
            </View>
            <View style={styles.boxContentWrapper}>
              <Text style={styles.boxTextHeader}>Wartość: </Text>
              <Text
                style={styles.boxTextValue}>{walletInfo.tokens.reduce((a, b) => +a + (+b.tokenValue), 0) / 100}</Text>
              <Text style={styles.boxTextHeader}>zł</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          navigation={this.props.navigation}
          style={styles.blueButton}
          onPress={this.onPress}>
          <Text style={styles.buttonText}>
            Szczegóły
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}
