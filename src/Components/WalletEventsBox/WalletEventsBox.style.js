import { Dimensions, StyleSheet } from "react-native";
import globalStylesheet from "../../Utils/globalStylesheet";

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: 5,
    width: WIDTH,
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'column',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 5
  },
  blueButton: {
    ...globalStylesheet.normalBlueButtonContainer,
    borderRadius: 0
  },
  imageContainer: {
    width: WIDTH/3,
    height: WIDTH/3,
  },
  boxContentWrapper: {
    display: 'flex',
    flexDirection: 'row'
  },
  eventName: { fontFamily: 'TwCenMT', fontSize: 23, color: 'black' },
  boxTextHeader: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 20,
  },
  boxTextValue: {
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 20,
    color: 'black'
  },
  buttonText: {
    ...globalStylesheet.normalBlueButtonText,
    fontSize: 18
  },
  wrapper: { margin: 10, justifyContent: 'center' }

});
