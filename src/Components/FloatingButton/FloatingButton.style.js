import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  button: withBottomButton => {
    return {
      width: 60,
      height: 60,
      zIndex: 9999,
      borderRadius: 50,
      position: 'absolute',
      bottom: withBottomButton ? 80 : 30,
      right: 30,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#0E7AF5',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.1,
      shadowRadius: 0,
      elevation: 6,
      borderWidth: 1,
      borderColor: 'white'
    }
  }
});
export default styles;
