import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import TransactionService from '../../../Services/TransactionsService';
import TransactionConsentView from './TransactionConsentView';
import globalStylesheet from "../../../Utils/globalStylesheet";


export default class TransactionConsentContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }

  sendConsent = (decision) => {
    const consent = {
      transactionId: this.props.navigation.getParam('transactionId'),
      decision: decision
    };
    TransactionService.postTransactionConsent(consent)
      .then(response => {
        this.props.navigation.navigate('TransactionResultView', { transactionResult: response.data.transaction.status });
      })
      .catch(error => {
        if (error.response.data != null) {
          if (error.response.data.message === "That user has no pending transaction") {
            this.props.navigation.navigate('TransactionResultView', { transactionResult: "NO_TRANSACTION" });
          }
        }
      });
  };


  render() {
    return (
      <View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onDidFocus={() => this.setState({ isLoading: false })}
        />
        {this.state.isLoading && <ActivityIndicator size="large" color="#ff9811"/>}
        {!this.state.isLoading &&
        <TransactionConsentView
          navigation={this.props.navigation}
          sendConsent={this.sendConsent}
          tokens={this.props.navigation.getParam('tokens')}
        />
        }

      </View>);
  }
}
