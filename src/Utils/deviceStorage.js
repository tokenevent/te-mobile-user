import { ToastAndroid } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

const deviceStorage = {
    saveItem: (key, value) => saveItem(key, value),
    getItem: key => getItem(key)
};
const saveItem = async (key, value) => {
    try {
        AsyncStorage.setItem(key, value);
    } catch (error) {
        ToastAndroid.show(error.message, ToastAndroid.SHORT);
    }
};

const getItem = async key => {
    let value = '';
    try {
        value = AsyncStorage.getItem(key) || '';
    } catch (error) {
        ToastAndroid.show(error.message, ToastAndroid.SHORT);
    }

    return value;
};
export default deviceStorage;
