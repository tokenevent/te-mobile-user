import React, { Component } from 'react';
import { ToastAndroid, View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import UserService from "../../../Services/UserService";
import LoadingIndicator from "../../commons/LoadingIndicator";
import WalletCodeView from "./WalletCodeView";
import TransactionService from '../../../Services/TransactionsService';
import { groupBy } from '../../../Utils/util-functions';
import globalStylesheet from "../../../Utils/globalStylesheet";

export default class WalletCodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      walletCodeInfo: null,
      seconds: 60,
      isLoading: true,
    }
  }

  subtractSeconds = () => {
    this.interval = setInterval(() => {
      if (this.state.seconds > 0) {
        if (this.state.seconds % 3 === 0) {
          this.checkTransactionStatus().then(response => {
            if (response.status === 200) {
              const tokens = groupBy(response.data.transaction.tokens, 'buyableTokenName');
              this.props.navigation.navigate('TransactionConsentView', {
                transactionId: response.data.transaction.id,
                tokens: tokens
              });
              clearInterval(this.interval);
            }
          })
            .catch(err => {
              console.log(err.response.data);
              if (err.response.status !== 409) {
                console.log('error', err)
              } else if (err.response.data.message === "That user doesn't have enough tokens") {
                  clearInterval(this.interval);
                  this.props.navigation.navigate('TransactionResultView', {
                  transactionResult: "NOT_ENOUGH_TOKENS"
                });
              }
            })
        }
        this.setState({
          seconds: this.state.seconds - 1
        });

      } else
        clearInterval(this.interval);
    }, 1000);
  };


  prepareData() {
    UserService.getWalletId()
      .then(response => this.walletCodeLoadSuccessHandler(response))
      .catch(error => this.walletCodeErrorHandler(error));
  }

  async checkTransactionStatus() {
    return await TransactionService.getCurrentTransaction();
  }

  walletCodeLoadSuccessHandler = response => {
    this.setState({
      walletCodeInfo: response.data,
      seconds: response.data.validForSeconds,
      isLoading: false
    });
    this.subtractSeconds();
  };

  walletCodeErrorHandler = error => {
    ToastAndroid.show("Wystąpił błąd podczas pobierania kodu portfela!", ToastAndroid.SHORT);
    console.log(error);
  };

  render() {
    const { isLoading } = this.state;

    return (
      <View style={globalStylesheet.fullFlex}>
        <NavigationEvents
          onWillFocus={() => {
            this.setState({ isLoading: true });
            this.prepareData();
          }}
          onWillBlur={() => clearInterval(this.interval)}
        />
        {isLoading && <LoadingIndicator/>}
        {!isLoading &&
        <WalletCodeView navigation={this.props.navigation} walletCodeInfo={this.state.walletCodeInfo}/>
        }
      </View>
    );
  }
}
