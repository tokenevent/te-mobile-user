import React from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './SettingsView.style';
import SettingsField from '../../SettingsField/SettingsField';
import globalStylesheet from './../../../Utils/globalStylesheet';

export default class SettingsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newData: Object.assign({}, this.props.userData),
      isSaveDisabled: true
    };
  }

  onChangeValue = (val, index) => {
    let temp = this.state.newData;
    temp[index] = val;
    this.setState({
      newData: temp,
      isSaveDisabled: true
    });

    Object.keys(temp).forEach(key => {
      if (temp[key] !== this.props.userData[key]) {
        this.setState({
          isSaveDisabled: false
        });
      }
    });

    if (index === "email" && !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(val)) {
      this.setState({
        isSaveDisabled: true
      });
    }
  };

  render() {
    const { newData, isSaveDisabled } = this.state;

    return (
      <View style={globalStylesheet.fullFlex}>
        <ScrollView>
          <View style={globalStylesheet.viewTitleWrapper}>
            <Text style={globalStylesheet.viewTitle}>Ustawienia</Text>
          </View>
            <SettingsField
              placeholder={"Email"}
              value={newData["email"]}
              onChangeValue={this.onChangeValue}
              index={"email"}
            />
            <SettingsField
              placeholder={"Telefon"}
              value={newData["phone"]}
              onChangeValue={this.onChangeValue}
              index={"phone"}
            />
            <SettingsField
              placeholder={"Imię"}
              value={newData["firstName"]}
              onChangeValue={this.onChangeValue}
              index={"firstName"}
            />
            <SettingsField
              placeholder={"Nazwisko"}
              value={newData["lastName"]}
              onChangeValue={this.onChangeValue}
              index={"lastName"}
            />
        </ScrollView>
        <TouchableOpacity
          style={styles.blueButton(isSaveDisabled)}
          onPress={() => this.props.updateData(newData)}
          disabled={isSaveDisabled}
        >
          <Text style={globalStylesheet.normalBlueButtonText}>
            Zapisz
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
