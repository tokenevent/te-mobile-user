import { api } from "../Utils/api-urls";
import { authorizeRequest } from "../Utils/requests";

const TransactionService = {
    getMyTransactions: () => getMyTransactions(),
    getCurrentTransaction: () => getCurrentTransaction(),
    getTransactionDetails: (id) => getTransactionDetails(id),
    postTransactionConsent: (transactionConset) => postTransactionConsent(transactionConset),
    getBuyTransactionDetails: (id) => getBuyTransactionDetails(id),
};

const getMyTransactions = () => {
    const url = api.MY_TRANSACTIONS;
    return authorizeRequest().then(a => a.get(url));
};

const getCurrentTransaction = () => {
    const url = api.TRANSACTION_CONSENT;
    return authorizeRequest().then(a => a.get(url));
};

const getTransactionDetails = (id) => {
    const url = api.TRANSACTION_DETAILS + "/" + id;
    return authorizeRequest().then(a => a.get(url));
};

const postTransactionConsent = (transactionConset) => {
    const url = api.TRANSACTION_CONSENT;
    return authorizeRequest().then(a => a.post(url, transactionConset));
};

const getBuyTransactionDetails = (id) => {
    const url = api.BUY_TRANSACTION_DETAILS + "/" + id + "/details";
    return authorizeRequest().then(a => a.get(url));
};

export default TransactionService;
