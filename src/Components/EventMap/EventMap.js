import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import styles from './EventMap.style';


const EventMap = ({ lat, lon, eventName }) => {

  return <><MapView
    style={styles.full}
    initialRegion={{
      latitude: Number(lat),
      longitude: Number(lon),
      latitudeDelta: 0.01,
      longitudeDelta: 0.01,
    }}
  >
    <Marker
      coordinate={{
        latitude: Number(lat),
        longitude: Number(lon)
      }}
      title={eventName}
    />
  </MapView>
  </>
};


export default EventMap;
