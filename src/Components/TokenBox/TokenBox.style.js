import { StyleSheet } from "react-native";

const styles = StyleSheet.create({


  mainWrapper: { width: '100%', marginLeft: 10, marginBottom: 20, marginRight: 10, marginTop: 10 },
  contentWrapper: { display: 'flex', flexDirection: 'column', marginRight: 20 },
  boxWrapper: { display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' },
  buttonsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  rowWrapper: { display: 'flex', flexDirection: 'row', alignItems: 'center' },
  availableAmount: {
    marginLeft: 3,
    marginRight: 3,
    fontFamily: 'TwCenMT-Condensed',
    fontSize: 18,
    textTransform: 'uppercase',
    color: 'black'
  }
});
export default styles
