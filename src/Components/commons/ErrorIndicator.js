import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default class ErrorIndicator extends Component {
    render() {
        return (
            <View style={[styles.container]}>
                <Text style={[styles.text]}>Wystąpił błąd</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: '55%'
    },
    text: {
        fontSize: 20
    }
});
