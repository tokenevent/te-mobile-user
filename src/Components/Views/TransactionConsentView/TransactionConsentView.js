import React, { Component } from 'react';
import { FlatList, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import globalStylesheet from '../../../Utils/globalStylesheet';
import { toPlnString } from "../../../Utils/util-functions";
import TransactionConsentBox from "../../TransactionConsentBox/TransactionConsentBox";
import styles from './TransactionConsentView.style';

export default class TransactionConsentView extends Component {
  constructor(props) {
    super(props);
  }

  onAccept = () => this.props.sendConsent("ACCEPT");
  onDecline = () => this.props.sendConsent("REJECT");

  render() {
    const tokens = this.props.tokens;
    let sum = 0;

    Object.keys(tokens).map(t => {
      sum += tokens[t][0].tokenValue * tokens[t].length;
    });

    return (
      <View style={styles.container}>
        <View style={globalStylesheet.viewTitleWrapper}>
          <Text style={globalStylesheet.viewTitle}>Weryfikacja transakcji</Text>
        </View>
        <ScrollView style={styles.scrollContent}>
          <FlatList
            data={Object.keys(tokens)}
            renderItem={({ item }) =>
              <TransactionConsentBox
                tokenName={tokens[item][0].buyableTokenName}
                unitPrice={tokens[item][0].tokenValue}
                amount={tokens[item].length}
              />
            }
            keyExtractor={(id) => id}
          />
        </ScrollView>
        <View style={globalStylesheet.borderTop}>
          <View style={globalStylesheet.summaryWrapper}>
            <Text style={globalStylesheet.summaryText}>Łączny koszt: </Text>
            <Text style={globalStylesheet.summaryAmount}>{toPlnString(sum)}</Text>
            <Text style={globalStylesheet.summaryCurrency}>zł</Text>
          </View>

          <View style={styles.cancelButton}>
            <TouchableOpacity
              style={styles.decline}
              onPress={this.onDecline}
            >
              <Text style={globalStylesheet.normalBlueButtonText}>
                Anuluj
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.accept}
            onPress={this.onAccept}
          >
            <Text style={globalStylesheet.normalBlueButtonText}>
              Akceptuj
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

