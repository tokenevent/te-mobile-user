import { Dimensions, StyleSheet } from "react-native";

const WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  buttonWrapper: (borderColor) => {
    return {
      height: 50,
      width: (WIDTH - 30) / 2,
      alignItems: "center",
      alignContent: "center",
      justifyContent: "center",
      borderWidth: 2,
      borderRadius: 5,
      borderColor: borderColor
    }
  },
  text: {
    fontFamily: "TwCenMT",
    fontWeight: "800",
    textTransform: "uppercase",
    color: "black",
    fontSize: 20
  }

});

export default styles;
