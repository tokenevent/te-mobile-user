import React, { Component } from 'react';
import { View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import TransactionHistoryListView from "./TransactionHistoryListView";
import TransactionService from "../../../Services/TransactionsService";
import LoadingIndicator from "../../commons/LoadingIndicator";
import ErrorIndicator from "../../commons/ErrorIndicator";
import styles from './TransactionHistoryView.style';
import FloatingButton from "../../FloatingButton/FloatingButton";

export default class TransactionHistoryView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transactions: [],
      isLoading: true,
      error: null,
      refreshList: false,
      isError: false
    }
  }

  getData() {
    TransactionService.getMyTransactions()
      .then(response => this.loadTransactions(response))
      .catch(error => this.handleError(error))
  }

  loadTransactions(httpResponse) {
    this.setState({
      transactions: httpResponse.data.transactions,
      isLoading: false,
      isError: false
    });
  }

  handleError(err) {
    this.setState({
      transactions: [],
      isLoading: false,
      error: err,
      isError: true
    })
  }

  getRejectedOnly = transaction => transaction.status === "REJECTED";
  getAcceptedOnly = transaction => transaction.status === "ACCEPTED";

  render() {
    const { isLoading, error, transactions, refreshList, isError } = this.state;

    return (
      <View style={styles.wrapper}>

        <NavigationEvents
          onWillFocus={() => {
            this.setState({ isLoading: true });
            this.getData();
          }}
        />
        {isLoading && <LoadingIndicator/>}
        {!isLoading && <><TransactionHistoryListView
          transactions={transactions}
          getRejectedOnly={this.getRejectedOnly}
          getAcceptedOnly={this.getAcceptedOnly}
          refreshList={refreshList}

        />
          <FloatingButton navigation={this.props.navigation}/>
        </>}

        {isError && !isLoading && <ErrorIndicator/>}
      </View>
    );
  }
};
