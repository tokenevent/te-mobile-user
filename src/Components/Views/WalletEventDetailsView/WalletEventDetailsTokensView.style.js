import { StyleSheet } from "react-native";
import globalStylesheet from "../../../Utils/globalStylesheet";

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1b5b96',
  },
  blueButton: {
    ...globalStylesheet.normalBlueButtonContainer,
    borderRadius: 0
  }
});
export default styles
