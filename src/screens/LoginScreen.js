import React, {Component,} from 'react';
import {
    ActivityIndicator,
    Image,
    StyleSheet,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from 'react-native';
import logoWhiteMedium from '../../assets/images/logoWhiteMedium.png'
import {isUsernameValid} from "./../Utils/validators";
import UserService from "./../Services/UserService";
import globalStylesheet from './../Utils/globalStylesheet';
import AsyncStorage from '@react-native-community/async-storage';

export default class LoginScreen extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            isLoading: false
        }
    }


    onLogin = () => {
        this.setState({isLoading: true});
        const {username, password} = this.state;
        if (isUsernameValid(username) && isUsernameValid(password)) {
            const userData = {
                username: username,
                password: password
            };
            UserService.loginUser(userData)
                .then(response => this.loginUserSuccessHandler(response))
                .catch(error => this.loginUserErrorHandler(error))
        }
    };

    onRegister = () => this.props.navigation.navigate('RegisterScreen');


    loginUserSuccessHandler = async response => {
        await AsyncStorage.setItem('accessToken', response.data);
        this.getUserData();
    };

    getUserData = () => {
        UserService.getUserData()
        .then(response => this.getUserDataSuccesHandler(response))
        .catch(error => this.getUserDataErrorHandler(error));
    };

    getUserDataSuccesHandler = async response => {
        const acc = response.data.user.account;
        await AsyncStorage.setItem('name', acc.firstName + " " + acc.lastName);
        await AsyncStorage.setItem('email', acc.email);
        this.setState({isLoading: false});
        this.props.navigation.navigate('App');
    };

    getUserDataErrorHandler = error => {
        ToastAndroid.show("Wystąpił błąd podczas pobierania informacji o użytkowniku", ToastAndroid.SHORT);
    };

    loginUserErrorHandler = error => {
        this.setState({isLoading: false});
        ToastAndroid.show("Wystąpił błąd podczas logowania...", ToastAndroid.SHORT);
        console.log(error);
    };

    render() {
        const {isLoading} = this.state;
        return (
            <View style={styles.container}>
                {!isLoading ?
                    <View>
                        <Image
                            style={styles.logo}
                            source={logoWhiteMedium}
                            resizeMode="contain"
                        />
                        <TextInput
                            style={globalStylesheet.normalTextInput}
                            placeholderTextColor="#77b3fb"
                            placeholder="Login..."
                            onChangeText={username => this.setState({username: username})}
                        />
                        <TextInput
                            secureTextEntry={true}
                            style={globalStylesheet.normalTextInput}
                            placeholderTextColor="#77b3fb"
                            placeholder="Hasło..."
                            onChangeText={password => this.setState({password: password})}
                        />
                        <TouchableOpacity onPress={this.onLogin}>
                            <View style={globalStylesheet.yellowButtonContainer}>
                                <Text style={globalStylesheet.yellowButtonText}>ZALOGUJ</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.onRegister}>
                            <View style={styles.registerBtn}>
                                <Text style={globalStylesheet.yellowButtonText}>ZAREJESTRUJ</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    :
                    <View>
                        <ActivityIndicator size="large" color="#ff9811"/>
                    </View>}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        ...globalStylesheet.container,
        alignItems: 'stretch',
        justifyContent: 'center',
        backgroundColor: '#084da0',
    },
    logo: {
        width: undefined,
        height: 80,
        margin: 15
    },
    registerBtn: {
        ...globalStylesheet.yellowButtonContainer,
        backgroundColor: '#0039a0',
    }
});
