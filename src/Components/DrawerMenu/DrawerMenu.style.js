import { Dimensions, StyleSheet } from 'react-native';

const HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  gradient: {
    flex: 1,
    height: HEIGHT - 25,
  },
  scroller: {
    flex: 1,
    backgroundColor: '#024492',
  },
  eventName: {
    fontSize: 13,
    paddingBottom: 5,
    paddingTop: 5,
    color: 'rgba(255,255,255,0.7)',
    textAlign: 'center',
  },
  profile: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 25,
    borderBottomColor: '#777777',
  },
  profileText: {
    flex: 3,
    marginTop: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  name: {
    fontSize: 20,
    paddingBottom: 5,
    color: 'white',
    textAlign: 'center',
  },
  city: {
    fontSize: 10,
    paddingBottom: 5,
    paddingTop: 5,
    color: 'rgba(255,255,255,0.7)',
    textAlign: 'center',
  },
  imgView: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  img: {
    height: 120,
    width: 120,
    borderRadius: 60,
    borderWidth: 10,
    borderColor: '#3A99C5',
  },
  topLinks: {
    height: 260,
    marginBottom: -50,
  },
  bottomLinks: {
    flex: 1,
    paddingTop: 20,
  },
  link: {
    flex: 1,
    fontSize: 19,
    padding: 6,
    paddingLeft: 29,
    margin: 5,
    fontFamily: 'TwCenMT',
    textAlign: 'left',
    color: 'white',
  },
  drawerMenuLink: {
    flex: 1,
    fontSize: 20,
    padding: 6,
    paddingLeft: 14,
    fontWeight: '300',
    textTransform: 'uppercase',
    margin: 5,
    fontFamily: 'TwCenMT',
    textAlign: 'left',
    color: 'white',
  },
  menuPosition: {
    height: 20,
    width: 20,
    marginRight: 3,
    opacity: 0.5,
  },
  menuPositionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pin: {
    height: 20,
    width: 13,
    marginRight: 5,
    opacity: 0.5,
  },
});

export default styles;
