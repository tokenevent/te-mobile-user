import React from 'react';
import { FlatList, Text, TouchableOpacity, View } from 'react-native';
import TransactionHistoryRow from "../../TransactionHistoryRow/TransactionHistoryRow";
import styles from './TransactionHistoryListView.style';
import globalStylesheet from "../../../Utils/globalStylesheet";

const TransactionHistoryListView = ({ transactions, getAcceptedOnly, getRejectedOnly }) => {
  const [refreshList, setRefreshList] = React.useState(false);
  const [renderTransactions, setRenderTransactions] = React.useState(transactions);
  const [buttonsHighlight, setButtonsHighlights] = React.useState([true, false, false]);

  return (
    <View style={styles.container}>
      <View style={globalStylesheet.viewTitleWrapper}>
        <Text style={globalStylesheet.viewTitle}>Historia transakcji</Text>
      </View>

      {transactions.length > 0 ?
        <View style={styles.filterListContainer}>
          <View style={styles.filterButtonsContainer}>
            <TouchableOpacity
              style={styles.filterButton(buttonsHighlight[0])}
              onPress={() => {
                setRenderTransactions(transactions);
                setRefreshList(!refreshList);
                setButtonsHighlights([true, false, false]);
              }}>
              <Text style={styles.filterButtonText}>
                Wszystkie
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.filterButton(buttonsHighlight[1])}
              onPress={() => {
                setRenderTransactions(transactions.filter(getAcceptedOnly));
                setRefreshList(!refreshList);
                setButtonsHighlights([false, true, false]);
              }}>
              <Text style={styles.filterButtonText}>
                Przyjęte
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.filterButton(buttonsHighlight[2])}
              onPress={() => {
                setRenderTransactions(transactions.filter(getRejectedOnly));
                setRefreshList(!refreshList);
                setButtonsHighlights([false, false, true]);
              }}>
              <Text style={styles.filterButtonText}>
                Odrzucone
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={renderTransactions}
            renderItem={({ item }) => <TransactionHistoryRow key={item.id} transaction={item} buttonsHighlight={buttonsHighlight}/>}
            keyExtractor={(item, index) => String(index)}
            extraData={refreshList}
          />
        </View>
        :
        <View style={{ marginLeft: 15 }}>
          <Text style={styles.noTokensText}>Przykro nam, nie masz żadnych transakcji.</Text>
        </View>
      }

    </View>
  );
};

export default TransactionHistoryListView;
