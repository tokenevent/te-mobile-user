import { api } from "../Utils/api-urls"
import { authorizeRequest } from "../Utils/requests";

const TokenService = {
    issueTokens: tokensData => issueTokens(tokensData)
};

const issueTokens = tokensData => {
    const url = api.ISSUE_TOKENS;
    return authorizeRequest().then(a => a.post(url, tokensData));
};

export default TokenService;
