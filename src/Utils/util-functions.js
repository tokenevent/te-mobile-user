export const toPlnString  = (value) => {
    return (value / 100).toLocaleString('pl-PL', {
        currency: 'PLN'
    });
};


export const groupBy = (list, props) => {
    return list.reduce((a, b) => {
        (a[b[props]] = a[b[props]] || []).push(b);
        return a;
    }, {});
};

