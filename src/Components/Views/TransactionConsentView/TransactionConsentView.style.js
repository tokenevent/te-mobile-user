import { StyleSheet } from "react-native";
import globalStylesheet from "../../../Utils/globalStylesheet";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  decline: {
    ...globalStylesheet.normalBlueButtonContainerWithoutRadius,
    backgroundColor: '#F23A3A',
  },
  accept: {
    ...globalStylesheet.normalBlueButtonContainerWithoutRadius,
    backgroundColor: '#53AA3E',
  },
  sumText: {
    color: '#0b3a66',
    fontSize: 32,
    margin: 15,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  scrollContent: {...globalStylesheet.fullFlex,
    margin: 10
  },
  cancelButton: { marginBottom: 15 }
});

export default styles;
