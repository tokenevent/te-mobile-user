import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  proceed: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1b5b96',
    color: 'white',
    height: 64,
    borderRadius: 10,
    padding: 10,
    margin: 15,
    width: '85%'
  },
  successImg: {
    alignSelf: 'center',
    width: 140,
    height: 100,
    margin: 30
  },
  failImg: {
    alignSelf: 'center',
    width: 120,
    height: 120,
    margin: 30
  },
  resultHeader: {
    color: 'black',
    fontFamily: 'TwCenMT',
    fontSize: 60,
    textAlign: 'center',
  },
  resultDesc: {
    fontFamily: 'TwCenMT',
    fontSize: 20,
    textAlign: 'center'
  },
  infoWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default styles;
