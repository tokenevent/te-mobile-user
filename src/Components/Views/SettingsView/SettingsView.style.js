import { Dimensions, StyleSheet } from "react-native";
import globalStylesheet from './../../../Utils/globalStylesheet';

const HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: HEIGHT - 223
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  text: {
    fontSize: 30,
  },
  blueButton: (disabled) => {
    return {
      ...globalStylesheet.normalBlueButtonContainer,
      borderRadius: 0,
      opacity: disabled ? 0.5 : 1
    }
  },
});
export default styles
