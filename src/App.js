import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Dimensions, Image } from 'react-native';
import LoginScreen from "./screens/LoginScreen";
import DrawerMenu from "./Components/DrawerMenu/DrawerMenu";
import MainViewRoute from "./Components/Views/MainEventsView/MainViewRoute";
import AuthLoadingScreen from "./screens/AuthLoadingScreen";
import styles from './Components/DrawerMenu/DrawerMenu.style';
import WalletViewRoute from "./Components/Views/WalletView/WalletViewRoute";
import EventDetailsRoute from "./Components/Views/EventDetailsView/EventDetailsRoute";
import WalletCodeRoute from "./Components/Views/WalletCodeView/WalletCodeRoute";
import WalletTokensDetailsRoute from "./Components/Views/WalletEventDetailsView/WalletTokensDetailsRoute";
import TransactionHistoryRoute from "./Components/Views/TransactionHistoryView/TransactionHistoryRoute";
import PurchaseHistoryRoute from "./Components/Views/PurchasesView/PurchaseHistoryRoute";
import BuyTokensRoute from "./Components/Views/BuyTokensForEventView/BuyTokensRoute";
import TransactionConsentRoute from "./Components/Views/TransactionConsentView/TransactionConsentRoute";
import TransactionResultRoute from "./Components/Views/TransactionResultView/TransactionResultRoute";
import PurchaseResultRoute from "./Components/PurchaseResult/PurchaseResultRoute";
import SettingsRoute from "./Components/Views/SettingsView/SettingsRoute";
import RegisterScreen from "./screens/RegisterScreen";
import { createStackNavigator } from "react-navigation-stack";
import OfflineRoute from './Components/Views/OfflineView/OfflineRoute';

const WIDTH = Dimensions.get('window').width;

const Hidden = () => null;


const AppStack = createDrawerNavigator({
    Main: {
      screen: MainViewRoute,
      navigationOptions: {
        drawerIcon: <Image style={styles.menuPosition} source={require('./../assets/images/calendarIco.png')}
                           resizeMode='stretch'/>,
        title: 'Wydarzenia'
      }
    },
    EventDetails: {
      screen: EventDetailsRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    Wallet: {
      screen: WalletViewRoute,
      navigationOptions: {
        drawerIcon: <Image style={styles.menuPosition} source={require('./../assets/images/walletIco.png')}
                           resizeMode='stretch'/>,
        title: 'Portfel'
      }
    },
    WalletCode: {
      screen: WalletCodeRoute,
      navigationOptions: {
        drawerIcon: <Image style={styles.menuPosition} source={require('./../assets/images/codeIco.png')}
                           resizeMode='stretch'/>,
        title: 'Kod portfela'
      }
    },
    Settings: {
      screen: SettingsRoute,
      navigationOptions: {
        drawerIcon: <Image style={styles.menuPosition} source={require('./../assets/images/settingsIco.png')}
                           resizeMode='stretch'/>,
        title: 'Ustawienia'
      }
    },
    PurchaseHistory: {
      screen: PurchaseHistoryRoute,
      navigationOptions: {
        drawerIcon: <Image style={styles.menuPosition} source={require('./../assets/images/coinsIco.png')}
                           resizeMode='stretch'/>,
        title: 'Historia zakupów'
      }
    },
    WalletTokensDetails: {
      screen: WalletTokensDetailsRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    TransactionHistory: {
      screen: TransactionHistoryRoute,
      navigationOptions: {
        drawerIcon: <Image style={styles.menuPosition} source={require('./../assets/images/historyIco.png')}
                           resizeMode='stretch'/>,
        title: 'Historia transakcji'
      }
    },
    BuyTokens: {
      screen: BuyTokensRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    TransactionConsent: {
      screen: TransactionConsentRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    TransactionResult: {
      screen: TransactionResultRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    PurchaseResult: {
      screen: PurchaseResultRoute,
      navigationOptions: {
        drawerLabel: <Hidden/>,
      },
    },
    Offline: {
      screen: OfflineRoute,
      navigationOptions: {
        drawerLockMode: 'locked-closed',
        drawerLabel: <Hidden/>,
      },
    }
  },
  {
    contentComponent: DrawerMenu,
    drawerWidth: WIDTH * 0.85,
  },
);

const AuthStack = createStackNavigator({ LoginView: LoginScreen, RegisterScreen: RegisterScreen });

const AppRoute = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: { screen: AppStack },
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);


export default createAppContainer(AppRoute);

